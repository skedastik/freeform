//
//  PPPBoundedElement.h
//  Freeform
//
//  Created by Alaric Holloway on 12/10/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "PPPElement.h"


@interface PPPBoundedElement : PPPElement

@property (nonatomic) int16_t swatchIndex;
@property (nonatomic) float height;
@property (nonatomic) float width;

@end
