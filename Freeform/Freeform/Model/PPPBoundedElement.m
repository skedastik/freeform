//
//  PPPBoundedElement.m
//  Freeform
//
//  Created by Alaric Holloway on 12/10/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPBoundedElement.h"


@implementation PPPBoundedElement

@dynamic swatchIndex;
@dynamic height;
@dynamic width;

@end
