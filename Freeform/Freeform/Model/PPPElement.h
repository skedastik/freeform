//
//  PPPElement.h
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PPPSpace;

@interface PPPElement : NSManagedObject

@property (nonatomic, retain) NSString * center;
@property (nonatomic) BOOL pinned;
@property (nonatomic) BOOL selected;
@property (nonatomic, retain) NSString * transform;
@property (nonatomic) int32_t zIndex;
@property (nonatomic) BOOL editable;
@property (nonatomic, retain) PPPSpace *space;

@end
