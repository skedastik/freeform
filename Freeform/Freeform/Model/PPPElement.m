//
//  PPPElement.m
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPElement.h"
#import "PPPSpace.h"


@implementation PPPElement

@dynamic center;
@dynamic pinned;
@dynamic selected;
@dynamic transform;
@dynamic zIndex;
@dynamic editable;
@dynamic space;

@end
