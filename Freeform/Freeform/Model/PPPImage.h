//
//  PPPImage.h
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "PPPBoundedElement.h"


@interface PPPImage : PPPBoundedElement

@property (nonatomic, retain) id maskPath;
@property (nonatomic, retain) NSString * sourcePathOrURL;

@end
