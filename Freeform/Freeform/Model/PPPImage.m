//
//  PPPImage.m
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPImage.h"


@implementation PPPImage

@dynamic maskPath;
@dynamic sourcePathOrURL;

@end
