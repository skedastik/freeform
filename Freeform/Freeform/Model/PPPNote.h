//
//  PPPNote.h
//  Freeform
//
//  Created by Alaric Holloway on 12/12/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "PPPBoundedElement.h"

@class PPPNote, PPPNoteTail;

@interface PPPNote : PPPBoundedElement

@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSSet *inLinks;
@property (nonatomic, retain) NSSet *outLinks;
@property (nonatomic, retain) NSSet *tails;
@end

@interface PPPNote (CoreDataGeneratedAccessors)

- (void)addInLinksObject:(PPPNote *)value;
- (void)removeInLinksObject:(PPPNote *)value;
- (void)addInLinks:(NSSet *)values;
- (void)removeInLinks:(NSSet *)values;

- (void)addOutLinksObject:(PPPNote *)value;
- (void)removeOutLinksObject:(PPPNote *)value;
- (void)addOutLinks:(NSSet *)values;
- (void)removeOutLinks:(NSSet *)values;

- (void)addTailsObject:(PPPNoteTail *)value;
- (void)removeTailsObject:(PPPNoteTail *)value;
- (void)addTails:(NSSet *)values;
- (void)removeTails:(NSSet *)values;

@end
