//
//  PPPNote.m
//  Freeform
//
//  Created by Alaric Holloway on 12/12/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPNote.h"
#import "PPPNote.h"
#import "PPPNoteTail.h"


@implementation PPPNote

@dynamic text;
@dynamic inLinks;
@dynamic outLinks;
@dynamic tails;

@end
