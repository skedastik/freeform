//
//  PPPNoteTail.h
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PPPNote;

@interface PPPNoteTail : NSManagedObject

@property (nonatomic, retain) NSString * position;
@property (nonatomic, retain) PPPNote *note;

@end
