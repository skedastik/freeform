//
//  PPPNoteTail.m
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPNoteTail.h"
#import "PPPNote.h"


@implementation PPPNoteTail

@dynamic position;
@dynamic note;

@end
