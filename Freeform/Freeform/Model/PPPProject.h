//
//  PPPProject.h
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PPPSpace;

@interface PPPProject : NSManagedObject

@property (nonatomic) NSTimeInterval dateCreated;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *spaces;

@property (nonatomic) NSDate *primitiveDateCreated;

@end

@interface PPPProject (CoreDataGeneratedAccessors)

- (void)addSpacesObject:(PPPSpace *)value;
- (void)removeSpacesObject:(PPPSpace *)value;
- (void)addSpaces:(NSSet *)values;
- (void)removeSpaces:(NSSet *)values;

@end
