//
//  PPPProject.m
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPProject.h"
#import "PPPSpace.h"


@implementation PPPProject

- (void)awakeFromInsert {
    [super awakeFromInsert];
    
    if (!self.dateCreated) {
        self.primitiveDateCreated = [NSDate date];
    }
}

@dynamic dateCreated;
@dynamic name;
@dynamic spaces;

@dynamic primitiveDateCreated;

@end
