//
//  PPPSpace.h
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PPPElement, PPPProject, PPPTheme;

@interface PPPSpace : NSManagedObject

@property (nonatomic) NSTimeInterval dateCreated;
@property (nonatomic) float height;
@property (nonatomic, retain) NSString * name;
@property (nonatomic) float width;
@property (nonatomic) BOOL readonly;
@property (nonatomic, retain) NSSet *elements;
@property (nonatomic, retain) PPPProject *project;
@property (nonatomic, retain) PPPTheme *theme;

@property (nonatomic) NSDate *primitiveDateCreated;

@end

@interface PPPSpace (CoreDataGeneratedAccessors)

- (void)addElementsObject:(PPPElement *)value;
- (void)removeElementsObject:(PPPElement *)value;
- (void)addElements:(NSSet *)values;
- (void)removeElements:(NSSet *)values;

@end
