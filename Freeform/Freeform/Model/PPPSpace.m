//
//  PPPSpace.m
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPSpace.h"
#import "PPPElement.h"
#import "PPPProject.h"
#import "PPPTheme.h"


@implementation PPPSpace

- (void)awakeFromInsert {
    [super awakeFromInsert];
    
    if (!self.dateCreated) {
        self.primitiveDateCreated = [NSDate date];
    }
}

@dynamic dateCreated;
@dynamic height;
@dynamic name;
@dynamic width;
@dynamic readonly;
@dynamic elements;
@dynamic project;
@dynamic theme;

@dynamic primitiveDateCreated;

@end
