//
//  PPPSwatch.h
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PPPTheme;

@interface PPPSwatch : NSManagedObject

@property (nonatomic, retain) NSString * color;
@property (nonatomic, retain) NSString * detailColor;
@property (nonatomic, retain) PPPTheme *theme;

@end
