//
//  PPPSwatch.m
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPSwatch.h"
#import "PPPTheme.h"


@implementation PPPSwatch

@dynamic color;
@dynamic detailColor;
@dynamic theme;

@end
