//
//  PPPTheme.h
//  Freeform
//
//  Created by Alaric Holloway on 12/11/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PPPSpace, PPPSwatch;

@interface PPPTheme : NSManagedObject

@property (nonatomic, retain) NSString * backgroundColor;
@property (nonatomic, retain) NSString * controlColor;
@property (nonatomic, retain) NSString * defaultTextColor;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * selectionColor;
@property (nonatomic, retain) NSString * superBackgroundColor;
@property (nonatomic, retain) NSSet *spaces;
@property (nonatomic, retain) NSOrderedSet *swatches;
@end

@interface PPPTheme (CoreDataGeneratedAccessors)

- (void)addSpacesObject:(PPPSpace *)value;
- (void)removeSpacesObject:(PPPSpace *)value;
- (void)addSpaces:(NSSet *)values;
- (void)removeSpaces:(NSSet *)values;

- (void)insertObject:(PPPSwatch *)value inSwatchesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromSwatchesAtIndex:(NSUInteger)idx;
- (void)insertSwatches:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeSwatchesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInSwatchesAtIndex:(NSUInteger)idx withObject:(PPPSwatch *)value;
- (void)replaceSwatchesAtIndexes:(NSIndexSet *)indexes withSwatches:(NSArray *)values;
- (void)addSwatchesObject:(PPPSwatch *)value;
- (void)removeSwatchesObject:(PPPSwatch *)value;
- (void)addSwatches:(NSOrderedSet *)values;
- (void)removeSwatches:(NSOrderedSet *)values;
@end
