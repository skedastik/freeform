//
//  PPPTheme.m
//  Freeform
//
//  Created by Alaric Holloway on 12/11/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPTheme.h"
#import "PPPSpace.h"
#import "PPPSwatch.h"


@implementation PPPTheme

@dynamic backgroundColor;
@dynamic controlColor;
@dynamic defaultTextColor;
@dynamic name;
@dynamic selectionColor;
@dynamic superBackgroundColor;
@dynamic spaces;
@dynamic swatches;

// As of 12/11/2013, Apple's generated setters for to-many ordered set attributes
// are completely broken. See:
//
// http://stackoverflow.com/questions/7385439/exception-thrown-in-nsorderedset-generated-accessors
//
// We use Owen Godfrey's implementation as a workaround.

- (void)addSwatches:(NSOrderedSet *)values {
    NSMutableOrderedSet *swatches = [[NSMutableOrderedSet alloc] initWithOrderedSet:self.swatches];
    
    if ([values isSubsetOfOrderedSet:swatches]) {
        return;
    }
    
    NSIndexSet * indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(swatches.count, values.count)];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexSet forKey:@"swatches"];
    [[self primitiveValueForKey:@"swatches"] unionOrderedSet:values];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexSet forKey:@"swatches"];
}

@end
