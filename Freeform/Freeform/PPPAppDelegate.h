//
//  PPPAppDelegate.h
//  Freeform
//
//  Created by Alaric Holloway on 11/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

+ (void)handleContextSaveError:(NSError *)error context:(NSManagedObjectContext *)context inController:(UIViewController *)controller;
+ (void)handlePerformFetchError:(NSError *)error inController:(UIViewController *)controller;

@end
