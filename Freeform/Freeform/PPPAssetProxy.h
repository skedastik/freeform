//
//  PPPThemeLoader.h
//  Freeform
//
//  Created by Alaric Holloway on 12/10/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//
//  PPPAssetProxy takes a PPPTheme object and generates reusable UIKit assets.
//  Only one instance of the proxy is used at a time.
//

#import <Foundation/Foundation.h>
#import "PPPTheme.h"

@interface PPPAssetProxy : NSObject

- (id)initWithTheme:(PPPTheme *)theme;

+ (UIColor *)getColorAtSwatchIndex:(NSUInteger)index;
+ (UIColor *)getDetailColorAtSwatchIndex:(NSUInteger)index;
+ (UIColor *)getBackgroundColor;
+ (UIColor *)getSuperBackgroundColor;
+ (UIColor *)getControlColor;
+ (UIColor *)getSelectionColor;
+ (UIColor *)getDefaultTextColor;

@end
