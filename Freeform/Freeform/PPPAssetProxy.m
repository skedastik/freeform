//
//  PPPThemeLoader.m
//  Freeform
//
//  Created by Alaric Holloway on 12/10/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPAssetProxy.h"
#import "PPPSwatch.h"
#import "UIColor+PPPExtensions.h"
#import "UIImage+PPPExtensions.h"

// Only one asset proxy is used at any given time.
static PPPAssetProxy *assetProxy = nil;


@interface PPPAssetProxy ()

@property (nonatomic) NSArray *colors;
@property (nonatomic) NSArray *detailColors;
@property (nonatomic) UIColor *backgroundColor;
@property (nonatomic) UIColor *superBackgroundColor;
@property (nonatomic) UIColor *controlColor;
@property (nonatomic) UIColor *selectionColor;
@property (nonatomic) UIColor *defaultTextColor;

@end


@implementation PPPAssetProxy

- (id)initWithTheme:(PPPTheme *)theme {
    self = [super init];
    
    if (self) {
        NSArray *swatches = [theme.swatches array];
        NSMutableArray *colors = [[NSMutableArray alloc] init];
        NSMutableArray *detailColors = [[NSMutableArray alloc] init];
        
        self.backgroundColor = [UIColor ppp_fromColorString:theme.backgroundColor];
        self.superBackgroundColor = [UIColor ppp_fromColorString:theme.superBackgroundColor];
        self.controlColor = [UIColor ppp_fromColorString:theme.controlColor];
        self.defaultTextColor = [UIColor ppp_fromColorString:theme.defaultTextColor];
        
        [swatches enumerateObjectsUsingBlock:^(PPPSwatch *swatch, NSUInteger idx, BOOL *stop) {
            [colors addObject:[UIColor ppp_fromColorString:swatch.color]];
            
            // Use default text color as detail color if no detail color is set
            UIColor *detailColor = (swatch.detailColor ? [UIColor ppp_fromColorString:swatch.detailColor] : self.defaultTextColor);
            [detailColors addObject:detailColor];
        }];
        
        self.colors = [colors copy];
        self.detailColors = [detailColors copy];
        
        UIImage *selectionPattern = [UIImage imageNamed:@"selection-pattern.png"];
        selectionPattern = [selectionPattern imageWithTint:[UIColor ppp_fromColorString:theme.selectionColor]];
        self.selectionColor = [UIColor colorWithPatternImage:selectionPattern];
        
        assetProxy = self;
    }
    
    return self;
}

+ (UIColor *)getColorAtSwatchIndex:(NSUInteger)index {
    return [assetProxy.colors objectAtIndex:index];
}

+ (UIColor *)getDetailColorAtSwatchIndex:(NSUInteger)index {
    return [assetProxy.detailColors objectAtIndex:index];
}

+ (UIColor *)getBackgroundColor {
    return assetProxy.backgroundColor;
}

+ (UIColor *)getSuperBackgroundColor {
    return assetProxy.superBackgroundColor;
}

+ (UIColor *)getControlColor {
    return assetProxy.controlColor;
}

+ (UIColor *)getSelectionColor {
    return assetProxy.selectionColor;
}

+ (UIColor *)getDefaultTextColor {
    return assetProxy.defaultTextColor;
}

@end
