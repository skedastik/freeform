//
//  PPPDynamicView.h
//  Freeform
//
//  Created by Alaric Holloway on 12/12/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//
//  A dynamic view is a view that appears in the Freeform workspace and
//  responds to gestures. See PPPElementView.h for more on gesture
//  handling.
//

#import <UIKit/UIKit.h>


@protocol PPPGestureHandler <NSObject>

@optional

- (void)concludeInteraction;
- (void)handlePan:(UIPanGestureRecognizer *)recognizer;
- (void)handlePinch:(UIPinchGestureRecognizer *)recognizer;
- (void)handlePress:(UILongPressGestureRecognizer *)recognizer;

@end


@interface PPPDynamicView : UIView <PPPGestureHandler>

@end
