//
//  PPPDynamicView.m
//  Freeform
//
//  Created by Alaric Holloway on 12/12/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPDynamicView.h"

@implementation PPPDynamicView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)concludeInteraction {}
- (void)handlePan:(UIPanGestureRecognizer *)recognizer {}
- (void)handlePinch:(UIPinchGestureRecognizer *)recognizer {}
- (void)handlePress:(UILongPressGestureRecognizer *)recognizer {}

@end
