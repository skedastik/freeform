//
//  PPPListViewController.h
//  Freeform
//
//  Created by Alaric Holloway on 11/25/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPPPulldownTableViewController.h"

@interface PPPEditableListViewController : PPPPulldownTableViewController <UIGestureRecognizerDelegate>

@end
