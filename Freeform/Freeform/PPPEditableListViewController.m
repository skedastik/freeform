//
//  PPPListViewController.m
//  Freeform
//
//  Created by Alaric Holloway on 11/25/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPEditableListViewController.h"

@interface PPPEditableListViewController ()

@end

@implementation PPPEditableListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    return [super initWithStyle:style];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
    tapRecognizer.cancelsTouchesInView = NO;
    tapRecognizer.delegate = self;
    [self.view addGestureRecognizer:tapRecognizer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Gesture recognizer actions and delegate

- (void)didTap:(UITapGestureRecognizer *)recognizer {
    // Remove focus from text view currently being edited if user touches outside of it
    [self.view endEditing:NO];
}

- (void)didActivatePulldown {
    [self.view endEditing:NO];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    // Do not intercept touch if user is tapping a text field's clear button.
    // Not the most robust solution but it works.
    return !([touch.view isKindOfClass:[UIButton class]] && [[touch.view superview] isKindOfClass:[UITextField class]]);
}

@end
