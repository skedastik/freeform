//
//  PPPElementView.h
//  Freeform
//
//  Created by Alaric Holloway on 11/13/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//
//  An element view is the view representation of any object that can be placed
//  into a Freeform workspace. These views can be manipulated with gestures and
//  report changes to the delegate.
//
//  GESTURE HANDLING
//
//  A key design pattern here is that elements do not create their own gesture
//  recognizers. This is the workspace view controller's responsibility. However,
//  elements do implement methods that respond to recognizer actions. In this
//  way we maintain MVC separation.
//
//  As a convention, all elements expect the delegate to allow simultaneous pan
//  and pinch recognition. Simultaneous pinch and long-press recognition should
//  NOT be allowed. Finally, all recognizers should be attached to the superview
//  of the element object (in other words, the workspace view, container of all
//  element objects).
//
//  Workspace gesture recognizer stub methods are implemented. They do nothing
//  by default and may be overridden as needed. By design, only the workspace
//  view controller responds to tap gestures.
//
//  Finally, concludeInteraction is called by the delegate when the interaction
//  should conclude (usually this is when all gestures have ended, but not
//  necessarily). The stub method does nothing by default. Override as needed.
//
//  SELECTION
//
//  The 'selected' property setter only sets the corresponding ivar. The setter
//  is overridden in PPPElement subclasses to also draw the element in its
//  selected state.
//
//  THEMES
//
//  The 'refreshedAppearance' property is a convenience property for updating
//  all elements via the UIAppearance proxy. It has a @dynamic implementation
//  with no ivar. Subclasses override its setter to update the element's
//  appearance using PPPAssetProxy. This is admittedly a bit hackish, but
//  Apple's appearance proxy is extremely inflexible. The alternative is to
//  iterate through all elements and adjust them manually. I find this equally
//  grotesque.
//

#import <UIKit/UIKit.h>
#import "PPPDynamicView.h"


@interface PPPElementView : PPPDynamicView

@property (nonatomic) BOOL selected;
@property (nonatomic) NSUInteger swatchIndex;
@property (nonatomic) UIColor *refreshedAppearance UI_APPEARANCE_SELECTOR;

@end
