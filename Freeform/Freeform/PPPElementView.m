//
//  PPPElementView.m
//  Freeform
//
//  Created by Alaric Holloway on 11/13/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPWorkspaceScrollView.h"
#import "PPPElementView.h"
#import <tgmath.h>

const CGFloat PPPElementSelectionBorderWidth = 8;


@implementation PPPElementView

@dynamic refreshedAppearance;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected {
    if (selected != _selected) {
        _selected = selected;
        
        if (selected) {
            self.layer.borderWidth = PPPElementSelectionBorderWidth;
        } else {
            self.layer.borderWidth = 0;
        }
    }
}

- (UIColor *)refreshedAppearance {
    return nil;
}

- (void)setRefreshedAppearance:(UIColor *)refreshedAppearance {}

@end
