//
//  PPPTableViewController.h
//  Freeform
//
//  Created by Alaric Holloway on 11/23/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//
//  An abstract class implementing fetched results controller delegate methods.
//
//  You must override the setter for the fetchedResultsController property to return
//  an NSFetchedResultsController initialized with a fetch request. 'performFetch'
//  will be called automatically in 'viewDidLoad'.
//
//  Perform dynamic table cell configuration in an overridden configureCell method.
//  This method should update the cell with changes to data objects (properties
//  that only need to be configured once can be configured in cellForRowAtIndexPath
//  after the cell is dequeued or created.
//

#import <UIKit/UIKit.h>

@interface PPPFetchTableViewController : UITableViewController {
    NSFetchedResultsController *_fetchedResultsController;
}

// Override the setter for this property to return an appropriately initialized NSFetchedResultsController
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;

// Configure or update an individual cell
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end
