//
//  PPPGestureMode.h
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#ifndef Freeform_PPPGestureMode_h
#define Freeform_PPPGestureMode_h

typedef enum {
    PPPGestureModeSelection,
    PPPGestureModeMultiselect,
    PPPGestureModeManipulation,
} PPPGestureMode;

#endif
