//
//  PPPMath.h
//  DynamicCallout
//
//  Created by Alaric Holloway on 11/11/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//
//  Math functions tailored specifically to this _non-critical_ application.
//

#import <Foundation/Foundation.h>

#pragma mark Floating point comparison

// Floating point comparison with optional epsilon argument
int feql(CGFloat x, CGFloat y);
int feql2(CGFloat x, CGFloat y, CGFloat epsilon);

// Returns true if x is between u and v inclusive, with optional epsilon argument
int fbtwn(CGFloat x, CGFloat u, CGFloat v);
int fbtwn2(CGFloat x, CGFloat u, CGFloat v, CGFloat epsilon);

#pragma mark - Geometry helper functions

// Return CGPoint at center of CGRect
CGPoint CGPointFromCenterOfRect(CGRect rect);

// Return a CGRect defined by two points
CGRect CGRectFromTwoPoints(CGPoint a, CGPoint b);

// Returns rect with minimum width and height enforced, but centered at the same position
CGRect CGRectEnforcingMinimumWidthAndHeight(CGRect rect, CGFloat minWidth, CGFloat minHeight);

// Returns rect centered at (cx,cy) with given width and height
CGRect CGRectMakeCentered(CGFloat cx, CGFloat cy, CGFloat width, CGFloat height);

// Returns an array of NSValue-wrapped CGPoint structs from the given CGPath representation of a polygon
NSArray *polygonFromCGPath(CGPathRef path);

// Return an NSMutableArray from an array of CGPoints w/ numPoints elements
NSMutableArray *NSMutableArrayFromCGPointArray(int numPoints, CGPoint *pointArray);

// Return the eccentricity of the given rect
CGFloat eccentricityOfRect(CGRect rect);

// Returns the first index matching CGPoint p in NSArray of NSValue-wrapped-CGPoints,
// or NSNotFound if not found.
NSUInteger indexOfPointInSet(NSArray *a, CGPoint p);

// Returns true if NSMutableArray 'a' has same NSValue-wrapped-CGPoints as array 'b'
int pointSetsAreEqual(NSArray *a, NSArray *b);

#pragma mark - NSMutableArray extensions

@interface NSMutableArray (PPPExtensions)

// Add CGPoints from given array of NSValue-wrapped-CGPoints that are not already in array instance
- (void)addUniqueCGPointsFromArray:(NSArray *)array;

@end

#pragma mark - Intersection functions

// Returns an NSMutableArray of intersections (NSValue wrapped CGPoints) between
// two line segments, or empty array if none found or lines coincide
NSMutableArray *intersectLineLine(CGPoint l1p1, CGPoint l1p2, CGPoint l2p1, CGPoint l2p2);

// Returns an NSMutableArray of intersections (NSValue wrapped CGPoints) between
// a line segment and a rect, or empty array if none found or  rects are equivalent.
NSMutableArray *intersectLineRect(CGPoint lp1, CGPoint lp2, CGRect rect);

// Returns an NSMutableArray of intersections (NSValue wrapped CGPoints) between
// a line segment and an arbitrary polygon represented by an NSArray of NSValue-
// wrapped CGPoints, or empty array if none found
NSMutableArray *intersectLinePolygon(CGPoint l1p1, CGPoint l1p2, NSArray *polygon);

// Returns an NSMutableArray of intersections (NSValue wrapped CGPoints) between
// a line segment and a circle, or empty array if none found
NSMutableArray *intersectLineCircle(CGPoint lp1, CGPoint lp2, CGPoint center, CGFloat r);

// Returns an NSMutableArray of intersections (NSValue wrapped CGPoints) between
// a circle and a rect, or empty array if none found
NSMutableArray *intersectCircleRect(CGPoint center, CGFloat r, CGRect rect);

// Returns an NSMutableArray of intersections (NSValue wrapped CGPoints) between
// a polygon and a rect, or empty array if none found
NSMutableArray *intersectPolygonRect(NSArray *polygon, CGRect rect);
