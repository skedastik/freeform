//
//  PPPMath.m
//  DynamicCallout
//
//  Created by Alaric Holloway on 11/11/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPMath.h"
#include <tgmath.h>

// Absolute epsilon value used to compare floating-point numbers (should be okay for most of
// our purposes). This is not a mission-critical application, so more robust floating-point
// comparison is not our concern.
static const CGFloat eps = 1e-3;

#pragma mark Floating point comparison

int feql2(CGFloat x, CGFloat y, CGFloat epsilon) {
    if (x == y) {
        return 1;
    }
    
    return fabs(x - y) < epsilon;
}

int feql(CGFloat x, CGFloat y) {
    return feql2(x, y, eps);
}

int fbtwn2(CGFloat x, CGFloat u, CGFloat v, CGFloat epsilon) {
    return x > fmin(u, v) - epsilon && x < fmax(u, v) + epsilon;
}

int fbtwn(CGFloat x, CGFloat u, CGFloat v) {
//    NSLog(@"fbtwn2(%f, %f %f) == %d", x, u, v, fbtwn2(x, u, v, eps));
    return fbtwn2(x, u, v, eps);
}

#pragma mark - CGGeometry helper functions

CGPoint CGPointFromCenterOfRect(CGRect rect) {
    return CGPointMake(rect.origin.x + rect.size.width / 2, rect.origin.y + rect.size.height / 2);
}

CGRect CGRectMakeCentered(CGFloat cx, CGFloat cy, CGFloat width, CGFloat height) {
    return CGRectMake(cx - width / 2, cy - height / 2, width, height);
}

CGRect CGRectEnforcingMinimumWidthAndHeight(CGRect rect, CGFloat minWidth, CGFloat minHeight) {
    return CGRectInset(rect, fmin(CGRectGetWidth(rect) - minWidth, 0) / 2, fmin(CGRectGetHeight(rect) - minHeight, 0) / 2);
}

CGRect CGRectFromTwoPoints(CGPoint a, CGPoint b) {
    return CGRectMake(fmin(a.x, b.x), fmin(a.y, b.y), fabs(a.x - b.x), fabs(a.y - b.y));
}

CGFloat eccentricityOfRect(CGRect rect) {
    CGFloat w = rect.size.width, h = rect.size.height, major = fmax(w, h);
    
    // Normalize width and height
    w /= major;
    h /= major;
    
    return w > h ? h/w - 1 : 1 - w/h;
}

// CGPath Applier function used by 'polygonFromCGPath'
void CGPathApplierCollectPolygonPoints (void *info, const CGPathElement *element) {
    NSMutableArray *points = (__bridge NSMutableArray *)info;
    
    CGPoint *elementPoints = element->points;
    CGPathElementType type = element->type;
    
    switch(type) {
        case kCGPathElementMoveToPoint:
        case kCGPathElementAddLineToPoint:
            [points addObject:[NSValue valueWithCGPoint:elementPoints[0]]];
            break;
        default:
            // Do nothing.
            break;
    }
}

NSArray *polygonFromCGPath(CGPathRef path) {
    NSMutableArray *polygon = [[NSMutableArray alloc] init];
    CGPathApply(path, (__bridge void *)(polygon), CGPathApplierCollectPolygonPoints);
    return [polygon copy];
}

NSMutableArray *NSMutableArrayFromCGPointArray(int numPoints, CGPoint *pointArray) {
    assert(numPoints > 0);
    
    NSMutableArray *points = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < numPoints; i++) {
        [points addObject:[NSValue valueWithCGPoint:pointArray[i]]];
    }
    
    return points;
}

NSUInteger indexOfPointInSet(NSArray *a, CGPoint p) {
    __block NSUInteger foundIndex = NSNotFound;
    
    [a enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        CGPoint q = [(NSValue *)obj CGPointValue];
        
        if (feql(p.x, q.x) && feql(p.y, q.y)) {
            foundIndex = idx;
            *stop = YES;
        }
    }];
    
    return foundIndex;
}

int pointSetsAreEqual(NSArray *a, NSArray *b) {
    if ([a count] != [b count]) {
//        NSLog(@"Point sets have different number of points.");
        return 0;
    }
    
    for (int i = 0; i < [a count]; i++) {
        CGPoint p = [(NSValue *)a[i] CGPointValue];
        
//        NSLog(@"%@ found? %@", NSStringFromCGPoint(p), indexOfPointInSet(b, p) == NSNotFound ? @"NO" : @"YES");
        
        if (indexOfPointInSet(b, p) == NSNotFound) {
            return 0;
        }
    }
    
    return 1;
}

#pragma mark - NSMutableArray extensions

@implementation NSMutableArray (PPPExtensions)

- (void)addUniqueCGPointsFromArray:(NSArray *)array {
    [array enumerateObjectsUsingBlock:^(NSValue *value, NSUInteger idx, BOOL *stop) {
        __block BOOL pointIsUnique = YES;
        CGPoint candidatePoint = [value CGPointValue];
        
        [self enumerateObjectsUsingBlock:^(NSValue *value, NSUInteger idx, BOOL *stop) {
            CGPoint point = [value CGPointValue];
            
            if (feql(point.x, candidatePoint.x) && feql(point.y, candidatePoint.y)) {
                pointIsUnique = NO;
                *stop = YES;
            }
        }];
        
        if (pointIsUnique) {
            [self addObject:value];
        }
    }];
}

@end

#pragma mark - Intersection functions

NSMutableArray *intersectLineLine(CGPoint l1p1, CGPoint l1p2, CGPoint l2p1, CGPoint l2p2) {
    NSMutableArray *intersections = [[NSMutableArray alloc] init];
    
    CGFloat a1 = l1p2.y - l1p1.y;
    CGFloat b1 = l1p1.x - l1p2.x;
    CGFloat c1 = a1 * l1p1.x + b1 * l1p1.y;
    
    CGFloat a2 = l2p2.y - l2p1.y;
    CGFloat b2 = l2p1.x - l2p2.x;
    CGFloat c2 = a2 * l2p1.x + b2 * l2p1.y;
    
    CGFloat det = a1 * b2 - a2 * b1;
    
    if (!feql(det, 0)) {
        CGFloat x0 = (b2 * c1 - b1 * c2) / det;
        CGFloat y0 = (a1 * c2 - a2 * c1) / det;
        
//        NSLog(@"intersectLineLine > candidate:(%f,%f)", x0, y0);
        
        if (fbtwn(x0, l1p1.x, l1p2.x) && fbtwn(y0, l1p1.y, l1p2.y)
            && fbtwn(x0, l2p1.x, l2p2.x) && fbtwn(y0, l2p1.y, l2p2.y))
        {
            [intersections addObject:[NSValue valueWithCGPoint:CGPointMake(x0, y0)]];
        }
    }
    
    return intersections;
}

NSMutableArray *intersectLineCircle(CGPoint lp1, CGPoint lp2, CGPoint center, CGFloat r) {
    NSMutableArray *intersections = [[NSMutableArray alloc] init];
    
    CGFloat dx1 = lp2.x - lp1.x;
    CGFloat dy1 = lp2.y - lp1.y;
    
    CGFloat dx2 = lp1.x - center.x;
    CGFloat dy2 = lp1.y - center.y;
    
    CGFloat a = dx1 * dx1 + dy1 * dy1;
    CGFloat b = 2 * (dx2 * dx1 + dy2 * dy1);
    CGFloat c = (dx2 * dx2 + dy2 * dy2) - r * r;
    
    CGFloat discriminant = b * b - 4 * a * c;
    
    if (feql2(discriminant, 0, 1e-25)) {
        CGFloat t = -b / (2 * a);
        
        if (fbtwn(t, 0, 1)) {
            [intersections addObject:[NSValue valueWithCGPoint:CGPointMake(lp1.x + t * dx1, lp1.y + t * dy1)]];
        }
    } else if (discriminant > 0) {
        CGFloat rootd = sqrtf(discriminant);
        CGFloat t1 = (-b + rootd) / (2 * a);
        CGFloat t2 = (-b - rootd) / (2 * a);
        
        if (fbtwn(t1, 0, 1)) {
            [intersections addObject:[NSValue valueWithCGPoint:CGPointMake(lp1.x + t1 * dx1, lp1.y + t1 * dy1)]];
        }
        
        if (fbtwn(t2, 0, 1)) {
            [intersections addObject:[NSValue valueWithCGPoint:CGPointMake(lp1.x + t2 * dx1, lp1.y + t2 * dy1)]];
        }
    }
    
    return intersections;
}

NSMutableArray *intersectLineRect(CGPoint lp1, CGPoint lp2, CGRect rect) {
    NSMutableArray *intersections = [[NSMutableArray alloc] init];
    
    CGPoint topLeft = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGPoint topRight = CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect));
    CGPoint bottomRight = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    CGPoint bottomLeft = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect));
    
//    NSLog(@"tl:%@, tr:%@, br:%@, bl:%@", NSStringFromCGPoint(topLeft), NSStringFromCGPoint(topRight), NSStringFromCGPoint(bottomRight), NSStringFromCGPoint(bottomLeft));
    
    [intersections addUniqueCGPointsFromArray:intersectLineLine(lp1, lp2, topLeft, topRight)];
    [intersections addUniqueCGPointsFromArray:intersectLineLine(lp1, lp2, topRight, bottomRight)];
    [intersections addUniqueCGPointsFromArray:intersectLineLine(lp1, lp2, bottomRight, bottomLeft)];
    [intersections addUniqueCGPointsFromArray:intersectLineLine(lp1, lp2, bottomLeft, topLeft)];
    
    return intersections;
}

NSMutableArray *intersectLinePolygon(CGPoint l1p1, CGPoint l1p2, NSArray *polygon) {
    NSMutableArray *intersections = [[NSMutableArray alloc] init];
    
//    NSLog(@"Line      -- l1p1:%@, l1p2:%@", NSStringFromCGPoint(l1p1), NSStringFromCGPoint(l1p2));
    
    for (int i = 0; i < polygon.count - 1; i++) {
//        NSLog(@"Poly edge -- l1p1:%@, l1p2:%@",
//              NSStringFromCGPoint([polygon[i] CGPointValue]),
//              NSStringFromCGPoint([polygon[i+1] CGPointValue]));
        [intersections addUniqueCGPointsFromArray:intersectLineLine(l1p1, l1p2,
                                                             [polygon[i] CGPointValue], [polygon[i+1] CGPointValue])];
    }
    
//    NSLog(@"Poly edge -- l1p1:%@, l1p2:%@\n\n",
//          NSStringFromCGPoint([polygon[polygon.count - 1] CGPointValue]),
//          NSStringFromCGPoint([polygon[0] CGPointValue]));
    
    [intersections addUniqueCGPointsFromArray:intersectLineLine(l1p1, l1p2,
                                                         [polygon[polygon.count - 1] CGPointValue], [polygon[0] CGPointValue])];
    
//    NSLog(@"# of intersections: %u\n\n", intersections.count);
    
    return intersections;
}

NSMutableArray *intersectCircleRect(CGPoint center, CGFloat r, CGRect rect) {
    NSMutableArray *intersections = [[NSMutableArray alloc] init];
    
    CGPoint topLeft = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGPoint topRight = CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect));
    CGPoint bottomRight = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    CGPoint bottomLeft = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect));
    
    [intersections addUniqueCGPointsFromArray:intersectLineCircle(topLeft, topRight, center, r)];
    [intersections addUniqueCGPointsFromArray:intersectLineCircle(topRight, bottomRight, center, r)];
    [intersections addUniqueCGPointsFromArray:intersectLineCircle(bottomRight, bottomLeft, center, r)];
    [intersections addUniqueCGPointsFromArray:intersectLineCircle(bottomLeft, topLeft, center, r)];
    
    return intersections;
}

NSMutableArray *intersectPolygonRect(NSArray *polygon, CGRect rect) {
    NSMutableArray *intersections = [[NSMutableArray alloc] init];
    
    CGPoint topLeft = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGPoint topRight = CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect));
    CGPoint bottomRight = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    CGPoint bottomLeft = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect));
    
    [intersections addUniqueCGPointsFromArray:intersectLinePolygon(topLeft, topRight, polygon)];
    [intersections addUniqueCGPointsFromArray:intersectLinePolygon(topRight, bottomRight, polygon)];
    [intersections addUniqueCGPointsFromArray:intersectLinePolygon(bottomRight, bottomLeft, polygon)];
    [intersections addUniqueCGPointsFromArray:intersectLinePolygon(bottomLeft, topLeft, polygon)];
    
    
//    NSLog(@"Polgyon-Rect Intersections:");
//    [intersections enumerateObjectsUsingBlock:^(NSValue *value, NSUInteger idx, BOOL *stop) {
//        NSLog(@"%@\n", NSStringFromCGPoint([value CGPointValue]));
//    }];
    
//    NSLog(@"# of intersections: %u\n\n", intersections.count);
    
    return intersections;
}
