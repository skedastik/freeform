//
//  PPPMathTest.m
//  DynamicCallout
//
//  Created by Alaric Holloway on 11/11/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPUnitTests.h"
#import "PPPMathTest.h"
#import "PPPMath.h"

//
// PPPMath
//
// int feql(CGFloat x, CGFloat y);
// int feql2(CGFloat x, CGFloat y, CGFloat epsilon);
// int fbtwn(CGFloat x, CGFloat u, CGFloat v);
// int fbtwn2(CGFloat x, CGFloat u, CGFloat v, CGFloat epsilon);
// NSUInteger indexOfPointInSet(NSArray *a, CGPoint p);
// int pointSetsAreEqual(NSArray *a, NSArray *b);
// NSMutableArray *intersectLineLine(CGPoint l1p1, CGPoint l1p2, CGPoint l2p1, CGPoint l2p2);
// NSMutableArray *intersectLineRect(CGPoint lp1, CGPoint lp2, CGRect rect);
// NSMutableArray *intersectLineCircle(CGPoint lp1, CGPoint lp2, CGPoint center, CGFloat r);
//
NSMutableArray *intersectCircleRect(CGPoint center, CGFloat r, CGRect rect);
//

void runMathTests(void) {
    runTest("feql(1.0, 1.0)", feql(1.0, 1.0));
    runTest("!feql(1.0, 1.1)", !feql(1.0, 1.1));
    runTest("feql2(1.0, 1.0005, .001)", feql2(1.0, 1.0005, .001));
    runTest("!feql2(1.002, 1.0005, .001)", !feql2(1.002, 1.0005, .001));
    
    printf("\n");
    
    runTest("fbtwn(1.0, 0, 2.0)", fbtwn(1.0, 0, 2.0));
    runTest("fbtwn(1.0, 2.0, 0)", fbtwn(1.0, 2.0, 0));
    runTest("fbtwn(1.0, 1.0, 1.0)", fbtwn(1.0, 1.0, 1.0));
    runTest("fbtwn(200, 200, 200)", fbtwn(200, 200, 200));
    runTest("!fbtwn(3.0, 0, 2.0)", !fbtwn(3.0, 0, 2.0));
    runTest("!fbtwn(3.0, 2.0, 0)", !fbtwn(3.0, 2.0, 0));
    
    printf("\n");
    
    CGPoint pa1[] = {{0,0}, {1,1}, {2,2}};
    NSMutableArray *points1 = NSMutableArrayFromCGPointArray(3, pa1);
    
    runTest("indexOfPointInSet({{0,0} {1,1} {2,2}}, {1,1}) == 1",
            indexOfPointInSet(points1, CGPointMake(1, 1)) == 1);
    
    runTest("indexOfPointInSet({{0,0} {1,1} {2,2}}, {3,3}) == NSNotFound",
            indexOfPointInSet(points1, CGPointMake(3, 3)) == NSNotFound
    );
    
    runTest("indexOfPointInSet({}, {1,1})                  == NSNotFound",
            indexOfPointInSet(@[], CGPointMake(1, 1)) == NSNotFound
    );
    
    printf("\n");
    
    CGPoint pa2[] = {{1,1}, {2,2}, {0,0}};
    CGPoint pa3[] = {{0,0}, {1,1}, {3,3}};
    NSMutableArray *points2 = NSMutableArrayFromCGPointArray(3, pa2);
    NSMutableArray *points3 = NSMutableArrayFromCGPointArray(3, pa3);

    runTest("pointSetsAreEqual({{0,0} {1,1} {2,2}}, {{1,1} {2,2} {0,0}})",
            pointSetsAreEqual(points1, points2)
    );
    
    runTest("!pointSetsAreEqual({{0,0} {1,1} {2,2}}, {{0,0} {1,1} {3,3}})",
            !pointSetsAreEqual(points1, points3)
    );
    
    runTest("!pointSetsAreEqual({{0,0} {1,1} {2,2}}, {{0,0} {1,1} {3,3}})",
            !pointSetsAreEqual(points1, points3)
            );
    
    runTest("pointSetsAreEqual({{0,0} {1,1} {2,2}}, {{0,0} {1,1} {2,2}})",
            pointSetsAreEqual(points1, points1)
            );
    
    printf("\n");
    
    NSMutableArray *intersections;
    
    CGPoint paExpected1[] = {{0,0}};
    points1 = NSMutableArrayFromCGPointArray(1, paExpected1);
    
    intersections = intersectLineLine(CGPointMake(0,1), CGPointMake(0,-1), CGPointMake(1,0), CGPointMake(-1,0));
    runTest("intersectLineLine({0,1}, {0,-1}, {1,0}, {-1,0})   => {{0,0}}",
            pointSetsAreEqual(intersections, points1)
    );
    
    intersections = intersectLineLine(CGPointMake(1,1), CGPointMake(-1,-1), CGPointMake(1,-1), CGPointMake(-1,1));
    runTest("intersectLineLine({1,1}, {-1,-1}, {1,-1}, {-1,1}) => {{0,0}}",
            pointSetsAreEqual(intersections, points1)
    );
    
    intersections = intersectLineLine(CGPointMake(1,1), CGPointMake(-1,-1), CGPointMake(-1,1), CGPointMake(0, 0));
    runTest("intersectLineLine({1,1}, {-1,-1}, {-1,1}, {0,0})  => {{0,0}}",
            pointSetsAreEqual(intersections, points1)
    );
    
    intersections = intersectLineLine(CGPointMake(0,1), CGPointMake(0,-1), CGPointMake(0,2), CGPointMake(0,-2));
    runTest("intersectLineLine({0,1}, {0,-1}, {0,2}, {0,-2})   => {}               // Parallel segments",
            [intersections count] == 0
    );
    
    intersections = intersectLineLine(CGPointMake(0,1), CGPointMake(0,-1), CGPointMake(0,1), CGPointMake(0,-1));
    runTest("intersectLineLine({0,1}, {0,-1}, {0,1}, {0,-1})   => {}               // Coinciding segments",
            [intersections count] == 0
    );
    
    intersections = intersectLineLine(CGPointMake(0,1), CGPointMake(0,-1), CGPointMake(-2,0), CGPointMake(-1, 0));
    runTest("intersectLineLine({0,1}, {0,-1}, {-2,0}, {-2,-1}) => {}               // Segments too short",
            [intersections count] == 0
    );
    
    printf("\n");
    
    CGPoint paExpected2[] = {{1,0}};
    intersections = intersectLineCircle(CGPointMake(0,0), CGPointMake(2,0), CGPointMake(0,0), 1);
    runTest("intersectLineCircle({0,0}, {2,0}, {0,0}, 1)       => {{1,0}}          // One intersection",
            pointSetsAreEqual(intersections, NSMutableArrayFromCGPointArray(1, paExpected2))
    );
    
    CGPoint paExpected3[] = {{0,1}, {0,-1}};
    intersections = intersectLineCircle(CGPointMake(0,2), CGPointMake(0,-2), CGPointMake(0,0), 1);
    runTest("intersectLineCircle({0,2}, {0,-2}, {0,0}, 1)      => {{0,1} {0,-1}}   // Two intersections",
            pointSetsAreEqual(intersections, NSMutableArrayFromCGPointArray(2, paExpected3))
    );
    
    CGPoint paExpected4[] = {{-1,0}};
    intersections = intersectLineCircle(CGPointMake(-1,2), CGPointMake(-1,-2), CGPointMake(0,0), 1);
    runTest("intersectLineCircle({-1,2}, {-1,-2}, {0,0}, 1)    => {{-1,0}}         // Tangency",
            pointSetsAreEqual(intersections, NSMutableArrayFromCGPointArray(1, paExpected4))
    );
    
    intersections = intersectLineCircle(CGPointMake(-3,0), CGPointMake(-2,0), CGPointMake(0,0), 1);
    runTest("intersectLineCircle({-3,0}, {-2,0}, {0,0}, 1)     => {}               // Segment too short",
            [intersections count] == 0
    );
    
    printf("\n");
    
    CGPoint paExpected5[] = {{-1,0}, {1,0}};
    intersections = intersectLineRect(CGPointMake(-2,0), CGPointMake(2,0), CGRectMake(-1, -1, 2, 2));
    runTest("intersectLineRect({-2,0}, {2,0}, {-1, -1, 2, 2})  => {{-1,0} {1,0}}   // Left and right edges",
            pointSetsAreEqual(intersections, NSMutableArrayFromCGPointArray(2, paExpected5))
    );
    
    CGPoint paExpected6[] = {{0,-1}, {0,1}};
    intersections = intersectLineRect(CGPointMake(0,-2), CGPointMake(0,2), CGRectMake(-1, -1, 2, 2));
    runTest("intersectLineRect({0,-2}, {0,2}, {-1, -1, 2, 2})  => {{0,-1} {0,1}}   // Top and bottom edges",
            pointSetsAreEqual(intersections, NSMutableArrayFromCGPointArray(2, paExpected6))
    );
    
    printf("\n");
    
    NSMutableArray *polygon = [[NSMutableArray alloc] init];
    [polygon addObject:[NSValue valueWithCGPoint:CGPointMake(-2, 0)]];
    [polygon addObject:[NSValue valueWithCGPoint:CGPointMake(0,  -2)]];
    [polygon addObject:[NSValue valueWithCGPoint:CGPointMake(2,  0)]];
    
    CGPoint paExpected9[] = {{-1,-1}, {1,-1}};
    intersections = intersectLinePolygon(CGPointMake(-2,-1), CGPointMake(2,-1), polygon);
    runTest("intersectLinePolygon({-2,-1}, {2,-1}, {{-2,0} {0,-2} {2,0}})           => {{-1,-1} {1,-1}}",
            pointSetsAreEqual(intersections, NSMutableArrayFromCGPointArray(2, paExpected9))
            );
    
    CGPoint paExpected10[] = {{-2,0}, {2,0}};
    intersections = intersectLinePolygon(CGPointMake(-3,0), CGPointMake(3,0), polygon);
    runTest("intersectLinePolygon({-3,0}, {3,0}, {{-2,0} {0,-2} {2,0}})             => {{-2,0} {2,0}}",
            pointSetsAreEqual(intersections, NSMutableArrayFromCGPointArray(2, paExpected10))
            );
    
    CGPoint paExpected11[] = {{0,-2}, {0,0}};
    intersections = intersectLinePolygon(CGPointMake(0,-3), CGPointMake(0,1), polygon);
    runTest("intersectLinePolygon({0,-3}, {0,1}, {{-2,0} {0,-2} {2,0}})             => {{0,-2} {0,0}}",
            pointSetsAreEqual(intersections, NSMutableArrayFromCGPointArray(2, paExpected11))
            );
    
    intersections = intersectLinePolygon(CGPointMake(-0.5,-0.5), CGPointMake(0.5,-0.5), polygon);
    runTest("intersectLinePolygon({-0.5,-0.5}, {0.5,-0.5}, {{-2,0} {0,-2} {2,0}})   => {}",
            intersections.count == 0
            );
    
    printf("\n");
    
    CGPoint paExpected7[] = {{-1,0}, {0,-1}, {1,0}, {0,1}};
    intersections = intersectCircleRect(CGPointMake(0,0), 1, CGRectMake(-1, -1, 2, 2));
    runTest("intersectCircleRect({0,0}, 1, {-1, -1, 2, 2})     => {{-1,0} {0,-1} {1,0} {0,1}}   // Circle inscribed in rect",
            pointSetsAreEqual(intersections, NSMutableArrayFromCGPointArray(4, paExpected7))
    );
    
    CGFloat d1 = sqrt(.19);
    CGFloat d2 = 0.9;
    CGPoint paExpected8[] = {{-d2,-d1}, {-d1,-d2}, {d1,-d2}, {d2,-d1}, {d2,d1}, {d1,d2}, {-d1,d2}, {-d2,d1}};
    intersections = intersectCircleRect(CGPointMake(0,0), 1, CGRectMake(-d2, -d2, d2*2, d2*2));
    runTest("intersectCircleRect({0,0}, 1, {...})              => {...}                         // Eight intersections",
            pointSetsAreEqual(intersections, NSMutableArrayFromCGPointArray(8, paExpected8))
    );
    
    printf("\n");
    
    CGPoint paExpected12[] = {{-1,-1}, {1,-1}, {1,0}, {-1,0}};
    intersections = intersectPolygonRect(polygon, CGRectMake(-1, -1, 2, 1));
    runTest("intersectLinePolygon({{-2,0} {0,-2} {2,0}}, {-1, -1, 2, 1})            => {{-1,-1} {1,-1} {1,0} {-1,0}}",
            pointSetsAreEqual(intersections, NSMutableArrayFromCGPointArray(4, paExpected12))
    );
    
    intersections = intersectPolygonRect(polygon, CGRectMake(5, 5, 1, 1));
    runTest("intersectLinePolygon({{-2,0} {0,-2} {2,0}}, {5, 5, 1, 1})              => {}",
            intersections.count == 0
    );
}
