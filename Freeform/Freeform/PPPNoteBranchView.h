//
//  PPPNoteBranchView.h
//  Freeform
//
//  Created by Alaric Holloway on 12/2/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//
//  Each note can have any number of branches. A branch is either a tail or a
//  link. A tail can point at anything (like the tail of a callout or dialogue
//  bubble in a comic strip). A link is a branch that points at another note
//  (e.g. the lines connecting nodes in a mind map).
//
//  Each PPPNoteBranchView instance is added as a direct subview of a note.
//

#import <UIKit/UIKit.h>
#import "PPPDynamicView.h"

@class PPPNoteView;
@class PPPNoteBranchView;


@protocol PPPNoteBranchViewDelegate <NSObject>

@required

// Invoked when long-press on branch begins
- (void)noteBranchViewDidWake:(PPPNoteBranchView *)branchView;

// Invoked when pan gesture on branch ends
- (void)noteBranchViewDidFinishMoving:(PPPNoteBranchView *)branchView;

// Invoked after branch view is flicked
- (void)noteBranchViewWasFlicked:(PPPNoteBranchView *)branchView;

@end


@interface PPPNoteBranchView : PPPDynamicView <PPPGestureHandler>

// Refine layer property to more specific type.
@property (nonatomic, readonly, retain) CAShapeLayer *layer;

@property (nonatomic) CGPoint tailPosition;

// This property determines whether the branch is a tail or a link. If 'nil', the
// branch is a tail (the position of which is contained in the 'tailPosition' property.
@property (nonatomic, weak) PPPNoteView *targetNote;

@property (nonatomic, weak) id<PPPNoteBranchViewDelegate> delegate;

// Update shape layer
- (void)redraw;

@end
