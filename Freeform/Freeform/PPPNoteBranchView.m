//
//  PPPNoteBranchView.m
//  Freeform
//
//  Created by Alaric Holloway on 12/2/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPNoteBranchView.h"
#import "PPPNoteView.h"
#import "PPPWorkspaceScrollView.h"
#import "PPPMath.h"
#import <tgmath.h>

static NSUInteger PPPNoteBranchViewCount = 0;

// The minimum velocity (points per second) at which a terminating pan
// gesture is considered a "flick".
const CGFloat PPPFlickThresholdVelocity = 2000;

// Thickness of a note's tail at its base
const CGFloat PPPNoteTailBaseThickness = PPPNoteCornerRadius / 1.5;

// insetRadius used for drawing tails ~ (noteCornerRadius * (1 - sin(M_PI_4)))
const CGFloat PPPNoteTailInset = PPPNoteCornerRadius * 0.29;

// Thickness of a note's link
const CGFloat PPPNoteLinkThickness = PPPNoteCornerRadius;

// insetRadius used for drawing links
const CGFloat PPPNoteLinkInset = PPPNoteLinkThickness / 2;


@interface PPPNoteBranchView ()

@property (nonatomic) BOOL obscured;

@end


@implementation PPPNoteBranchView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        NSLog(@"      *** Note branch view allocated (instance count: %u)", ++PPPNoteBranchViewCount);
        
        self.layer.strokeColor = nil;
        self.layer.fillColor = nil;
        self.layer.fillRule = kCAFillRuleEvenOdd;
        self.layer.lineWidth = PPPNoteLinkThickness;
        self.layer.lineCap = kCALineCapRound;
        
        self.obscured = NO;
    }
    
    return self;
}

- (void)dealloc {
    NSLog(@"      *** Note branch view deallocated (instance count: %u)", --PPPNoteBranchViewCount);
}

// The branch is drawn using a CAShapeLayer
+ (Class)layerClass {
    return [CAShapeLayer class];
}

- (void)didMoveToSuperview {
    [self updateDrawingStyle];
}

- (void)setTailPosition:(CGPoint)tailPosition {
    _tailPosition = tailPosition;
    [self redraw];
}

- (void)setTargetNote:(PPPNoteView *)targetNote {
    _targetNote = targetNote;
    
    // If target note is 'nil' assume that tail position will be set and redrawn.
    if (_targetNote) {
        [self redraw];
    }
}

- (void)redraw {
    //
    // Case 1
    //
    // Before the branch can be drawn we need to calculate the rect containing it. The
    // rect is defined by three things: the center of the note (Q) the target position
    // (P), and a rectangle inset within the note frame. The reason we use an inset
    // rect is because the note has rounded corners. The rect is inset by an amount
    // equal to the note's rounding radius.
    //
    //                 ___P
    //                 | /|            O - Origin of note view's coordinate system (0,0)
    //        O________|/_|__
    //        |        /I   |          Q - Center of note
    //        |       /     |
    //        |      Q      |          P - Target coordinates, relative to Q
    //        |             |
    //        |_____________|          I - Intersection of PQ with inset rect
    //
    //
    // Points I and P are almost enough to define our branch rect, but there is a small
    // complication: the branch has thickness at its base. Call this thickness 'r'. We
    // define a circle C with radius r centered at I.
    //
    //             ______P
    //             |    /|
    //             |   / |
    //             |__/  |
    //      _______/_/\__|_            J, K - Intersections of circle C with inset rect
    //           J \/_/ K
    //             /
    //            /
    //           /
    //          /
    //
    // Now, points P and J define our branch's rectangle. All of this can be easily
    // achieved using PPPMath's intersection functions and CGGeometry functions.
    //
    //
    // Case 2
    //
    // The above logic assumes that the branch is a tail. If it is a link, we need to
    // handle the following situation:
    //
    //                      R
    //                     /              S - Center of parent note
    //                    /
    //           _______I/____
    //              |   /|                R - Center of target note
    //              |  / |
    //              | /  |
    //      ________|/___|__              I - Intersection with parent note's inset rect
    //              /J
    //             /
    //            /                       J - Intersection with target note's inset rect
    //           /
    //          S
    //
    // This is simpler. We simply draw a line from I to J.
    //
    
    [self updateDrawingStyle];
    
    if (self.targetNote) {
        [self redrawLink];
    } else {
        [self redrawTail];
    }
}

- (void)redrawTail {
    //
    // Case 1's algorithm for drawing the tail works well in most cases, but in some
    // situations, it generates a very narrow and unpleasing tail shape. Specifically, it
    // breaks down for notes with highly eccentric rects where the tail position falls
    // close to the diagonal formed by the frame's corners:
    //
    //                          P
    //                           \
    //                           |
    //                            \_____
    //                             |   |
    //                             |\  |
    //                             ||  |          P - Tail postion
    //                             | \ |
    //                             | C |          C - Center of note
    //                             |   |
    //                             |   |
    //                             |___|
    //
    //
    // To solve this problem we define two focii for eccentric rectangles. The tail is drawn
    // from one of these focii instead of the center of the rect.
    //
    //                          P
    //                           \
    //                            \_
    //                             _\___
    //                         ___ | \ |              P - Tail postion
    //                          |  | F |
    //                        r |  |   |              C - Center of note
    //                         _|_ | . | C
    //                             |   |          F, F' - Focii
    //                             |   |
    //                             | F'|
    //                             |___|
    //
    //
    // These focii are set at a distance, r, from the center of the rect. The value of r
    // depends on the eccentricity of the note rect.
    //
    // Eccentricity is defined as follows:
    //
    //                              w > h,   h'/w' - 1
    //                       e = {
    //                              w <= h,  1 - w'/h'
    //
    // Where w' and h' are the normalized width and height of the rect (normalized such that
    // the longest edge is always equal to 1). By this definition, a rect with |e| equal to 1
    // is infinitely eccentric (a line segment), and a rect with eccentricity equal to 0 has
    // no eccentricity (a square). Note that e is in [-1,0] if w > h. This allows us to
    // differentiate the eccentricity of an x-by-y rect from a y-by-x rect.
    //
    // The eccentricity, e, of the note rect and the eccentricity, e', of the rect formed by P
    // and C define an eccentricity bias, e-bias:
    //
    //                              |e'| > |e|,   1
    //                  e-bias = {
    //                              |e'| <= |e|,  min(max(e'/e, 0), 1)
    //
    // So if the rect formed by P and C is as-eccentric or more-eccentric than the note rect,
    // then e-bias is 1. Note that e-bias is confined to [0,1]. This handles the case where
    // the sign of e and e' differ.
    //
    // Now we define r based on two radii, r-absolute-maximum (r-amax), and r-special-maximum
    // (r-smax).
    //
    //                                 _____
    //             ____________________|_. |
    //              |       ___________|_. |
    //       r-amax | r-smax |     ____|_. |
    //             _|_      _|_  r _|_ | . |          C - Center of note
    //                                 |  C|
    //                                 |   |
    //                                 |   |
    //                                 |___|
    //
    //
    // The distance r-amax is defined such that the focii are pushed out to the edge of the
    // note's inset rect. r-smax is the farthest extent of the focii for any given rect and
    // is defined by:
    //
    //                  r-smax = r-amax * |e|
    //
    // Now we can finally define r:
    //
    //                       r = r-smax * e-bias
    //
    // By this definition, a square note will always have r equal to 0, meaning that the tail
    // is always drawn from the center. Eccentric notes will have higher r when the tail is
    // positioned close to a diagonal formed by the note's corners, thereby guaranteeing a
    // nice tail shape.
    //
    // Note that there are always two possible focii. It is trivial to choose the correct one.
    //
    
    CGPoint focus = self.superview.center;
    
    CGFloat e     = eccentricityOfRect(self.superview.frame);
    CGFloat ep    = eccentricityOfRect(CGRectFromTwoPoints(self.superview.center, _tailPosition));
    CGFloat ebias = fmin(fmax(ep / e, 0), 1);
    
    CGRect insetRect = CGRectInset(self.superview.frame, PPPNoteTailInset, PPPNoteTailInset);
    
    if (e >= 0) {
        // Note is taller than it is wide, focii range vertically from center
        CGFloat ramax = insetRect.size.height / 2;
        CGFloat rsmax = ramax * fabs(e);
        CGFloat r     = rsmax * ebias;
        focus.y      += _tailPosition.y > focus.y ? r : -r;
    } else {
        // Note is wider than it is tall, focii range horizontally from center
        CGFloat ramax = insetRect.size.width / 2;
        CGFloat rsmax = ramax * fabs(e);
        CGFloat r     = rsmax * ebias;
        focus.x      += _tailPosition.x > focus.x ? r : -r;
    }
    
    // Make sure the insetRect width and height are larger than circle C radius.
    insetRect = CGRectEnforcingMinimumWidthAndHeight(insetRect, PPPNoteTailBaseThickness, PPPNoteTailBaseThickness);
    
    // Calculate intersection I in diagram above.
    NSMutableArray *intersections = intersectLineRect(focus, _tailPosition, insetRect);
    
    if (intersections.count > 0) {
        // The tail position is outside the frame, so we need to draw the tail
        
        // Use intersections of circle C centered at I to find intersections J and K.
        intersections = intersectCircleRect([intersections[0] CGPointValue], PPPNoteTailBaseThickness, insetRect);
        
        if (intersections.count > 1) {
            // The tail position is outside the frame so it is not obscured
            self.obscured =  NO;
            
            CGPoint j = [intersections[0] CGPointValue], k = [intersections[1] CGPointValue];
            
            // Union of rects defined by P, J, and K yield tail rect.
            CGRect jRect = CGRectFromTwoPoints(j, _tailPosition);
            CGRect kRect = CGRectFromTwoPoints(k, _tailPosition);
            CGRect tailRect = CGRectUnion(jRect, kRect);
            
            // Right now, tail rect is in superview's coordinate system. Transform it to note view's.
            tailRect = CGRectOffset(tailRect, -self.superview.frame.origin.x, -self.superview.frame.origin.y);
            
            // Redraw the tail shape, keeping in mind that points P, J, and K from the diagram above need
            // to be transformed to an untranslated coordinate system. The offset is easily calculated by
            // summing the tail rect's origin and the note's view's frame's origin.
            CGFloat dx = CGRectGetMinX(tailRect) + CGRectGetMinX(self.superview.frame);
            CGFloat dy = CGRectGetMinY(tailRect) + CGRectGetMinY(self.superview.frame);
            
            // Calculate P', J', and K' from offsets
            CGPoint pp = CGPointMake(_tailPosition.x - dx, _tailPosition.y - dy);
            CGPoint jp = CGPointMake(j.x - dx, j.y - dy);
            CGPoint kp = CGPointMake(k.x - dx, k.y - dy);
            
            UIBezierPath *path = [UIBezierPath bezierPath];
            [path moveToPoint:pp];
            [path addLineToPoint:jp];
            [path addLineToPoint:kp];
            [path closePath];
            
            self.frame = tailRect;
            self.layer.path = path.CGPath;
        }
    } else {
        // The tail is inside the frame...
        self.obscured =  YES;
        
        // ...so clear the shape layer
        if (self.layer.path) {
            self.layer.path = NULL;
        }
    }
}

- (void)redrawLink {
    CGPoint targetPosition = self.targetNote.center;
    
    // Calculate target note's inset rect
    CGRect targetInsetRect = CGRectInset(self.targetNote.frame, PPPNoteLinkInset, PPPNoteLinkInset);
    
    // Make sure inset rect width and height are larger than circle C radius.
    targetInsetRect = CGRectEnforcingMinimumWidthAndHeight(targetInsetRect, PPPNoteTailBaseThickness, PPPNoteTailBaseThickness);
    
    // Calculate intersection with target note frame.
    NSMutableArray *intersections = intersectLineRect(self.superview.center, targetPosition, targetInsetRect);
    
    // Assume the branch is obscured by the parent note
    self.obscured = YES;
    
    if (intersections.count > 0) {
        CGPoint i = [intersections[0] CGPointValue];
    
        // If the intersection is not inside parent note's frame, we need to draw the branch
        if (!CGRectContainsPoint(self.superview.frame, i)) {
                
            // Calculate parent note's inset rect
            CGRect parentInsetRect = CGRectInset(self.superview.frame, PPPNoteLinkInset, PPPNoteLinkInset);
            parentInsetRect = CGRectEnforcingMinimumWidthAndHeight(parentInsetRect, PPPNoteTailBaseThickness, PPPNoteTailBaseThickness);
            
            // Calculate intersection with parent note frame.
            NSMutableArray *intersections = intersectLineRect(self.superview.center, targetPosition, parentInsetRect);
            
            if (intersections.count > 0) {                
                CGPoint j = [intersections[0] CGPointValue];
                    
                // The target position is outside the frame so it is not obscured
                self.obscured =  NO;
                
                // Branch rect is defined by I and J
                CGRect branchRect = CGRectFromTwoPoints(i, j);
                
                // Right now, tail rect is in superview's coordinate system. Transform it to note view's.
                branchRect = CGRectOffset(branchRect, -self.superview.frame.origin.x, -self.superview.frame.origin.y);
                
                // Redraw the branch shape, keeping in mind that points J, K, L, and M from the diagram above
                // need to be transformed to an untranslated coordinate system. The offset is easily calculated
                // by summing the branch rect's origin and the parent note's origin.
                CGFloat dx = CGRectGetMinX(branchRect) + CGRectGetMinX(self.superview.frame);
                CGFloat dy = CGRectGetMinY(branchRect) + CGRectGetMinY(self.superview.frame);
                
                // Calculate J', K', L', and M' from offsets
                CGPoint ip = CGPointMake(i.x - dx, i.y - dy);
                CGPoint jp = CGPointMake(j.x - dx, j.y - dy);
                
                UIBezierPath *path = [UIBezierPath bezierPath];
                [path moveToPoint:ip];
                [path addLineToPoint:jp];
                
                self.frame = branchRect;
                self.layer.path = path.CGPath;
            }
        }
    }
    
    if (self.obscured) {
        // Clear the shape layer if branch is obscured
        if (self.layer.path) {
            self.layer.path = NULL;
        }
    }
}

- (void)updateDrawingStyle {
    if (self.targetNote) {
        // If note is a link, render using stroke, not fill
        if (self.layer.fillColor) {
            self.layer.fillColor = nil;
            self.layer.strokeColor = [self.superview.backgroundColor CGColor];
        }
    } else if (!self.layer.fillColor) {
        // If not is a tail, render using fill, not stroke
        self.layer.fillColor = [self.superview.backgroundColor CGColor];
        self.layer.strokeColor = nil;
    }
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    // Hit test using a hit rect, and test against branch's shape path
    CGRect hitRect = [PPPWorkspaceScrollView getHitTestRectCenteredAt:point];
    return (!self.obscured && intersectPolygonRect(polygonFromCGPath(self.layer.path), hitRect).count) ? YES : NO;
}

#pragma mark - Gesture recognizers

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint vector = [(UIPanGestureRecognizer *)recognizer velocityInView:recognizer.view];
        CGFloat velocity = sqrt(vector.x * vector.x + vector.y * vector.y);
        BOOL isFlicking = velocity >= PPPFlickThresholdVelocity ? YES : NO;
        
        if (isFlicking) {
            [self.delegate noteBranchViewWasFlicked:self];
        } else {
            [self.delegate noteBranchViewDidFinishMoving:self];
        }
    } else {
        self.tailPosition = [recognizer locationInView:recognizer.view];
    }
}

- (void)handlePress:(UILongPressGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        [self.delegate noteBranchViewDidWake:self];
    }
}

@end
