//
//  PPPNoteView.h
//  Freeform
//
//  Created by Alaric Holloway on 11/10/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//
//  A note is a dynamic Freeform text element. Its text can be changed, its frame
//  can be resized and repositioned.
//
//  Notes also have accessories referred to as "branches". Branches are either
//  "tails" or "links". Links are visual links to other notes (as in a mind map).
//  Tails are like links to arbitrary locations in the Freeform workspace (as in
//  a call-out).
//
//  IMPLEMENTATION
//
//  A branch is represented by the PPPNoteBranchView class. This class supports
//  both tails and links. If a branch instance is a link, its 'targetNote'
//  property is non-nil and points at another PPPNoteView instance. If a branch
//  is a tail, its 'targetNote' property is nil, and the tail's position is held
//  in its 'tailPosition' property.
//
//  Each PPPNoteView can have any number of branches--links or tails. Links _to_
//  other notes are referred to as "out-links". Links _from_ other notes are
//  referred to as "in-links". Each PPPNoteView tracks its out-links implicitly
//  via its 'branches' property. In-links are tracked explicitly by its 'inLinks'
//  property.
//

#import <UIKit/UIKit.h>
#import "PPPElementView.h"
#import "PPPNoteBranchView.h"
#import "PPPNote.h"


// The corner radius of the callout
static const CGFloat PPPNoteCornerRadius = 8;

// Minimum dimensions of callout
static const CGFloat PPPNoteMinWidth = PPPNoteCornerRadius * 2.5;
static const CGFloat PPPNoteMinHeight = PPPNoteCornerRadius * 2.5;

// Inset of the internal UITextLabel
static const CGFloat PPPNoteTextInsetX = 8;
static const CGFloat PPPNoteTextInsetY = 4;

// Minimum width and height of internal UITextLabel
static const CGFloat PPPNoteLabelMinWidth = 12;
static const CGFloat PPPNoteLabelMinHeight = 20;

// Minimum width and height of callout
static const CGFloat PPPMinimumNoteSize = PPPNoteCornerRadius * 2;

// Maximum font size and corresponding minimum scale of callout text
static const CGFloat PPPMaximumNoteFontSize = 72;
static const CGFloat PPPMinimumNoteFontScale = .25;

// If the tail is positioned within this distance of the callout's frame, it is removed.
static const CGFloat PPPNoteTailAbsorbDistance = 5;


@protocol PPPNoteViewDelegate <NSObject>

@required

// Invoked after a note is resized
- (void)noteViewWasResized:(PPPNoteView *)noteView;

// Invoked when a pan gesture on a note's branch ends
- (void)noteView:(PPPNoteView *)noteView tailDidMove:(PPPNoteBranchView *)branchView;

// Invoked when note adds a tail in response to a long-press
- (void)noteView:(PPPNoteView *)noteView didAddTailInResponseToPress:(PPPNoteBranchView *)branchView;

// Invoked when note is about to remove a branch in response to a flick
- (void)noteView:(PPPNoteView *)noteView willRemoveBranchInResponseToFlick:(PPPNoteBranchView *)branchView;

// Invoked when note is about to absorb a branch
- (void)noteView:(PPPNoteView *)noteView willAbsorbBranch:(PPPNoteBranchView *)branchView;

@end


@interface PPPNoteView : PPPElementView <PPPNoteBranchViewDelegate>

@property (nonatomic) NSString *text;

@property (nonatomic, readonly) NSUInteger numberOfOutLinks;
@property (nonatomic, readonly) NSUInteger numberOfTails;

@property (nonatomic, weak) id<PPPNoteViewDelegate> delegate;

- (PPPNoteBranchView *)addLinkTo:(PPPNoteView *)noteView;
- (PPPNoteBranchView *)addTailWithPosition:(CGPoint)position;

- (NSUInteger)indexOfTailWithPosition:(CGPoint)position;

// Any out-links that aren't in noteViewArray are removed. New out-links are
// generated for any note views in noteViewArray that aren't already linked.
- (void)updateOutLinksWithArray:(NSArray *)noteViewArray;

// Tail corollary to 'updateOutLinksWithArray' method. 'tailPositionArray'
// is an array of string representations of CGPoint structs.
- (void)updateTailsWithArray:(NSArray *)tailPositionStringArray;

- (void)linkBranch:(PPPNoteBranchView *)branchView to:(PPPNoteView *)noteView;
- (void)unlinkBranch:(PPPNoteBranchView *)branchView;

@end
