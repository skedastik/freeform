//
//  PPPNoteView.m
//  Freeform
//
//  Created by Alaric Holloway on 11/10/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

// TODO: Optimize UILabel text display.
//
// UILabel typesetting/text-rendering is very CPU-intensive. Loading many
// UILabel objects with lots of text can take seconds (potentially tens of seconds
// when dealing with hundreds of labels). There are three optimizations to consider:
//
// 1) Set adjustsFontSizeToFitWidth property of labels to NO when loading text since
// the font size is saved as part of the model. Set it to YES once the note is re-
// sized. Text rendering is orders of magnitude slower when adjustsFontSizeToFitWidth
// is YES.
//
// 2) Hide text while panning or zooming.
//
// 3) Render text on a separate thread. This is way down the road if ever.

#import "UIView+PPPExtensions.h"
#import "PPPNoteView.h"
#import "PPPMath.h"
#import "PPPAssetProxy.h"
#import "PPPNoteTail.h"
#import <tgmath.h>

static NSUInteger PPPNoteViewCount = 0;


@interface PPPNoteView ()

@property (nonatomic) UILabel *textLabel;

@property (nonatomic) NSMutableArray *branches;
@property (nonatomic) NSMutableArray *inLinks;

// Gesture state
@property (nonatomic) CGPoint panTranslation;
@property (nonatomic) CGPoint initialPosition;
@property (nonatomic) CGRect initialTouchRect;
@property (nonatomic) CGRect initialFrame;
@property (nonatomic) NSUInteger lastNumberOfTouches;

@end


@implementation PPPNoteView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        NSLog(@"      *** Note view allocated (instance count: %u)", ++PPPNoteViewCount);
        
        self.layer.cornerRadius = PPPNoteCornerRadius;
        self.layer.borderColor = [[PPPAssetProxy getSelectionColor] CGColor];
        self.backgroundColor = [PPPAssetProxy getColorAtSwatchIndex:self.swatchIndex];
        
        self.lastNumberOfTouches = 0;
        
        self.branches = [[NSMutableArray alloc] init];
        self.inLinks = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void)dealloc {
    NSLog(@"      *** Note view deallocated (instance count: %u)", --PPPNoteViewCount);
}

- (void)didMoveToSuperview {
    if (self.superview) {
        NSLog(@"      PPPNoteView -> didMoveToSuperview (index: %u)", [self.superview.subviews indexOfObject:self]);
    }
    [super didMoveToSuperview];
}

- (void)removeFromSuperview {
    [super removeFromSuperview];
    
    // Unlink all out-links
    [self.branches enumerateObjectsUsingBlock:^(PPPNoteBranchView *branchView, NSUInteger idx, BOOL *stop) {
        [self unlinkBranch:branchView];
    }];
    
    // Tell in-links to remove link to this note
    [self.inLinks enumerateObjectsUsingBlock:^(PPPNoteView *noteView, NSUInteger idx, BOOL *stop) {
        [noteView removeLinkTo:self];
    }];
}

- (void)setSwatchIndex:(NSUInteger)swatchIndex {
    if (swatchIndex != self.swatchIndex) {
        [super setSwatchIndex:swatchIndex];
        
        self.backgroundColor = [PPPAssetProxy getColorAtSwatchIndex:self.swatchIndex];
        
        if (_textLabel) {
            _textLabel.textColor = [PPPAssetProxy getDetailColorAtSwatchIndex:self.swatchIndex];
        }
    }
}

- (void)setRefreshedAppearance:(UIColor *)refreshedAppearance {
    self.layer.borderColor = [[PPPAssetProxy getSelectionColor] CGColor];
    
    // Force setSwatchIndex to refresh the note's appearance
    NSUInteger currentSwatchIndex = self.swatchIndex;
    [super setSwatchIndex:NSIntegerMax];
    [self setSwatchIndex:currentSwatchIndex];
}

// Update the internal text/label view's frame to match the note's frame
- (void)updateTextFrame {
    if (_textLabel && !_textLabel.hidden) {
        CGRect textRect = CGRectInset(self.bounds, PPPNoteTextInsetX, PPPNoteTextInsetY);
        _textLabel.frame = CGRectEnforcingMinimumWidthAndHeight(textRect, PPPNoteLabelMinWidth, PPPNoteLabelMinHeight);
    }
}

- (void)setText:(NSString *)text {
    if (![text isEqualToString:self.text]) {
        _text = text;
        
        if (self.text) {
            if (!_textLabel) {
                _textLabel = [[UILabel alloc] init];
                
                _textLabel.textColor                 = [PPPAssetProxy getDetailColorAtSwatchIndex:self.swatchIndex];
                _textLabel.numberOfLines             = 0;
                _textLabel.adjustsFontSizeToFitWidth = YES;
                _textLabel.font                      = [UIFont systemFontOfSize:PPPMaximumNoteFontSize];
                _textLabel.minimumScaleFactor        = PPPMinimumNoteFontScale;
                
                [self addSubview:_textLabel];
            }
            
            _textLabel.text = self.text;
            [self updateTextFrame];
        } else {
            if (_textLabel) {
                [_textLabel removeFromSuperview];
                _textLabel = nil;
            }
        }
    }
}

- (void)setFrame:(CGRect)frame {
    frame = CGRectEnforcingMinimumWidthAndHeight(frame, PPPNoteMinWidth, PPPNoteMinHeight);
    
    if (!CGRectEqualToRect(frame, self.frame)) {
        [super setFrame:frame];
        [self redrawAllBranches];
        [self updateTextFrame];
    }
}

- (void)setCenter:(CGPoint)center {
    if (!CGPointEqualToPoint(center, self.center)) {
        [super setCenter:center];
        [self redrawAllBranches];
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    __block UIView *hitView = nil;
    
    if ([self pointInside:point withEvent:event]) {
    // TODO: Fix bug where tap on note gets absorbed by overlying notes' branch views.
    
        hitView = self;
    } else {
        [self.branches enumerateObjectsUsingBlock:^(PPPNoteBranchView *branchView, NSUInteger idx, BOOL *stop) {
            if ([branchView pointInside:[self convertPoint:point toView:branchView] withEvent:event]) {
                hitView = branchView;
                *stop = YES;
            }
        }];
    }
    
    return hitView;
}

#pragma mark - Branches

// This method is the "inverse" of addLinkTo and should only be called by addLinkTo.
- (void)addInLink:(PPPNoteView *)noteView {
    [self.inLinks addObject:noteView];
}

// This method should only be called by removeBranch
- (void)removeInLink:(PPPNoteView *)noteView {
    [self.inLinks removeObject:noteView];
}

// This method should only be called by addTailWithPosition or addLinkTo 
- (PPPNoteBranchView *)addBranch {
    PPPNoteBranchView *branchView = [[PPPNoteBranchView alloc] init];
    
    // Insert branch views below the text label
    [self insertSubview:branchView atIndex:0];
    [self.branches addObject:branchView];
    branchView.delegate = self;
    
    // A new branch is a tail by default.
    _numberOfTails++;
    
    NSLog(@"      PPPNoteView -> addBranch  (out-links: %u, tails: %u)", self.numberOfOutLinks, self.numberOfTails);
    
    return branchView;
}

- (void)removeBranchAtIndex:(NSUInteger)index {
    if (index < self.branches.count) {
        PPPNoteBranchView *branchView = self.branches[index];
        [self unlinkBranch:branchView];
        [branchView removeFromSuperview];
        [self.branches removeObjectAtIndex:index];
    }
}

- (void)removeBranch:(PPPNoteBranchView *)branchView {
    NSUInteger index = [self.branches indexOfObject:branchView];
    
    if (index != NSNotFound) {
        [self removeBranchAtIndex:index];
    }
}

// Returns YES if branch was absorbed, NO otherwise
- (BOOL)absorbBranch:(PPPNoteBranchView *)branchView {
    CGRect absorbRect = CGRectInset(self.frame, -PPPNoteTailAbsorbDistance, -PPPNoteTailAbsorbDistance);
    
    // If branch is a tail and its position is within the note frame, remove it
    if (!branchView.targetNote && CGRectContainsPoint(absorbRect, branchView.tailPosition)) {
        [self.delegate noteView:self willAbsorbBranch:branchView];
        [self removeBranch:branchView];
        
        return YES;
    }
    
    return NO;
}

- (void)linkBranch:(PPPNoteBranchView *)branchView to:(PPPNoteView *)noteView {
    [self unlinkBranch:branchView];
    
    if (noteView != self) {
        if ([self hasLinkTo:noteView]) {
            // Link already exists so branch is redundant. Remove it.
            [self removeBranch:branchView];
        } else {
            branchView.targetNote = noteView;
            _numberOfOutLinks++;
            _numberOfTails--;
            
            NSLog(@"      PPPNoteView -> linkBranch (out-links: %u, tails: %u)", self.numberOfOutLinks, self.numberOfTails);
            
            // Connect inverse link
            [noteView addInLink:self];
        }
    }
}

- (void)unlinkBranch:(PPPNoteBranchView *)branchView {
    if (branchView.targetNote) {
        [branchView.targetNote removeInLink:self];
        branchView.targetNote = nil;
        _numberOfOutLinks--;
        _numberOfTails++;
        
        NSLog(@"         PPPNoteView -> unlinkBranch (out-links: %u, tails: %u)", self.numberOfOutLinks, self.numberOfTails);
    }
}

- (NSUInteger)indexOfBranchTo:(PPPNoteView *)noteView {
    __block NSUInteger index = NSNotFound;
    
    [self.branches enumerateObjectsUsingBlock:^(PPPNoteBranchView *branchView, NSUInteger idx, BOOL *stop) {
        if (branchView.targetNote == noteView) {
            index = idx;
            *stop = YES;
        }
    }];
    
    return index;
}

- (PPPNoteBranchView *)findBranchTo:(PPPNoteView *)noteView {
    NSLog(@"         findBranchTo note view w/ index %u", [noteView.superview.subviews indexOfObject:noteView]);
    return self.branches[[self indexOfBranchTo:noteView]];
}

- (BOOL)hasLinkTo:(PPPNoteView *)noteView {
    if ([self.inLinks containsObject:noteView]) {
        return YES;
    }
    
    for (int i = 0; i < self.branches.count; i++) {
        if (((PPPNoteBranchView *)(self.branches[i])).targetNote == noteView) {
            return YES;
        }
    }
    
    return NO;
}

- (PPPNoteBranchView *)addTailWithPosition:(CGPoint)position {
    PPPNoteBranchView *branchView = [self addBranch];
    branchView.tailPosition = position;
    return branchView;
}

- (PPPNoteBranchView *)addLinkTo:(PPPNoteView *)noteView {
    PPPNoteBranchView *branchView = [self addBranch];
    [self linkBranch:branchView to:noteView];
    return branchView;
}

- (void)updateOutLinksWithArray:(NSArray *)noteViewArray {
    if (!(noteViewArray.count == 0 && self.numberOfOutLinks == 0)) {
        // We want to minimize updates and recreations of branch views. This means first
        // determining which branch views are obsolete. We reuse as many of these
        // obselete branch views as possible, generating new ones only when we run out of
        // obselete ones. If any obsolete branches have not been reused, we discard them.
        
        NSMutableIndexSet *nonObseleteBranchViewIndices = [[NSMutableIndexSet alloc] init];
        NSMutableArray *newOutLinks = [[NSMutableArray alloc] init];
        
        // Iterate through note view array and collect non-redundant out-links
        [noteViewArray enumerateObjectsUsingBlock:^(PPPNoteView *noteView, NSUInteger idx, BOOL *stop) {
            NSUInteger foundIndex = [self indexOfBranchTo:noteView];
            if (foundIndex == NSNotFound) {
                [newOutLinks addObject:noteView];
            } else {
                [nonObseleteBranchViewIndices addIndex:foundIndex];
            }
        }];
        
        NSInteger delta = newOutLinks.count - self.numberOfOutLinks + nonObseleteBranchViewIndices.count;
        NSLog(@"      Configuring out-links of note at z-index %u (delta = %d)", [self.superview.subviews indexOfObject:self], delta);
        
        // If delta is 0, nothing has changed and we should return immediately
        if (delta != 0) {
            _numberOfOutLinks += delta;
            
            NSMutableArray *allBranches = [[NSMutableArray alloc] init];
            __block NSUInteger newOutLinksLeftIndex = 0;
            __block NSUInteger branchesLeftIndex = 0;
            
            // Iterate through branches, reusing as many obsolete ones as possible
            [self.branches enumerateObjectsUsingBlock:^(PPPNoteBranchView *branchView, NSUInteger idx, BOOL *stop) {
                if (newOutLinksLeftIndex == newOutLinks.count) {
                    *stop = YES;
                } else {
                    // Skip over non-obsolete branches
                    if (![nonObseleteBranchViewIndices containsIndex:idx]) {
                        // Reuse obsolete branches
                        branchView.targetNote = newOutLinks[newOutLinksLeftIndex];
                        newOutLinksLeftIndex++;
                    }
                    
                    // Collect non-redundant and reused branches
                    [allBranches addObject:branchView];
                    
                    branchesLeftIndex++;
                }
            }];
            
            if (branchesLeftIndex == self.branches.count) {
                // There are no reusable branches left, so generate new branches for remaining out-links.
                for (NSUInteger i = newOutLinksLeftIndex; i < newOutLinks.count; i++) {
                    [allBranches addObject:[self addLinkTo:newOutLinks[i]]];
                }
            } else {
                // There are leftover obselete branches. Remove them from superview.
                for (NSUInteger i = branchesLeftIndex; i < self.branches.count; i++) {
                    // Skip over non-obsolete branches
                    if (![nonObseleteBranchViewIndices containsIndex:i]) {
                        [(PPPNoteBranchView *)self.branches[i] removeFromSuperview];
                    }
                }
            }
            
            self.branches = allBranches;
        }
    }
}

- (void)updateTailsWithArray:(NSArray *)tailPositionStringArray {
    if (!(tailPositionStringArray.count == 0 && self.numberOfTails == 0)) {
        // We want to minimize updates and recreations of branch views. This means first
        // determining which branch views are obsolete. We reuse as many of these
        // obselete branch views as possible, generating new ones only when we run out of
        // obselete ones. If any obsolete branches have not been reused, we discard them.
        
        NSMutableIndexSet *nonObseleteBranchViewIndices = [[NSMutableIndexSet alloc] init];
        NSMutableArray *newTails = [[NSMutableArray alloc] init];
        
        // Iterate through note view array and collect non-redundant tails
        [tailPositionStringArray enumerateObjectsUsingBlock:^(NSString *tailPositionString, NSUInteger idx, BOOL *stop) {
            CGPoint tailPosition = CGPointFromString(tailPositionString);
            
            NSUInteger foundIndex = [self indexOfTailWithPosition:tailPosition];
            if (foundIndex == NSNotFound) {
                [newTails addObject:[NSValue valueWithCGPoint:tailPosition]];
            } else {
                [nonObseleteBranchViewIndices addIndex:foundIndex];
            }
        }];
        
        NSInteger delta = newTails.count - self.numberOfTails + nonObseleteBranchViewIndices.count;
        NSLog(@"      Configuring note tails (delta = %d)", delta);
        
        // If delta is 0, nothing has changed and we should return immediately
        if (delta != 0) {
            _numberOfTails += delta;
            
            NSMutableArray *allBranches = [[NSMutableArray alloc] init];
            __block NSUInteger newTailsLeftIndex = 0;
            __block NSUInteger branchesLeftIndex = 0;
            
            // Iterate through branches, reusing as many obsolete ones as possible
            [self.branches enumerateObjectsUsingBlock:^(PPPNoteBranchView *branchView, NSUInteger idx, BOOL *stop) {
                if (newTailsLeftIndex == newTails.count) {
                    *stop = YES;
                } else {
                    // Skip over non-obsolete branches
                    if (![nonObseleteBranchViewIndices containsIndex:idx]) {
                        // Reuse obsolete branches
                        branchView.tailPosition = [newTails[newTailsLeftIndex] CGPointValue];
                        newTailsLeftIndex++;
                    }
                    
                    // Collect non-redundant and reused branches
                    [allBranches addObject:branchView];
                    
                    branchesLeftIndex++;
                }
            }];
            
            if (branchesLeftIndex == self.branches.count) {
                // There are no reusable branches left, so generate new branches for remaining out-links.
                for (NSUInteger i = newTailsLeftIndex; i < newTails.count; i++) {
                    [allBranches addObject:[self addTailWithPosition:[newTails[i] CGPointValue]]];
                }
            } else {
                // There are leftover obselete branches. Remove them from superview.
                for (NSUInteger i = branchesLeftIndex; i < self.branches.count; i++) {
                    // Skip over non-obsolete branches
                    if (![nonObseleteBranchViewIndices containsIndex:i]) {
                        [(PPPNoteBranchView *)self.branches[i] removeFromSuperview];
                    }
                }
            }
            
            self.branches = allBranches;
        }
    }
}

- (void)removeLinkTo:(PPPNoteView *)noteView {
    [self removeBranchAtIndex:[self indexOfBranchTo:noteView]];
}

- (NSUInteger)indexOfTailWithPosition:(CGPoint)position {
    __block NSUInteger index = NSNotFound;
    
    [self.branches enumerateObjectsUsingBlock:^(PPPNoteBranchView *branchView, NSUInteger idx, BOOL *stop) {
        if (!branchView.targetNote && CGPointEqualToPoint(branchView.tailPosition, position)) {
            index = idx;
            *stop = YES;
        }
    }];
    
    return index;
}

- (PPPNoteBranchView *)findTailWithPosition:(CGPoint)position {
    NSLog(@"         findTailWithPosition: %@", NSStringFromCGPoint(position));
    return self.branches[[self indexOfTailWithPosition:position]];
}

- (void)redrawBranchTo:(PPPNoteView *)noteView {
    [[self findBranchTo:noteView] redraw];
}

- (void)redrawAllBranches {
    // Redraw all tails and out-links
    [self.branches enumerateObjectsUsingBlock:^(PPPNoteBranchView *branchView, NSUInteger idx, BOOL *stop) {
        [branchView redraw];
    }];
    
    // Tell in-links to redraw link to self
    [self.inLinks enumerateObjectsUsingBlock:^(PPPNoteView *noteView, NSUInteger idx, BOOL *stop) {
        [noteView redrawBranchTo:self];
    }];
}

#pragma mark - Branch view delegate

- (void)noteBranchViewDidWake:(PPPNoteBranchView *)branchView {
    [self unlinkBranch:branchView];
}

- (void)noteBranchViewDidFinishMoving:(PPPNoteBranchView *)branchView {
    if (![self absorbBranch:branchView]) {
        [self.delegate noteView:self tailDidMove:branchView];
    }
}

- (void)noteBranchViewWasFlicked:(PPPNoteBranchView *)branchView {
    [self.delegate noteView:self willRemoveBranchInResponseToFlick:branchView];
    [self removeBranch:branchView];
}

#pragma mark - Gesture recognition

- (void)concludeInteraction {
    [self didStopPinching];
}

- (void)didStopPinching {
    // Pinching ended, so show text label and update its frame
    _textLabel.hidden = NO;
    [self updateTextFrame];
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    self.panTranslation = [recognizer translationInView:recognizer.view];

    if (recognizer.state == UIGestureRecognizerStateEnded) {
        [self updateTextFrame];
        [self.delegate noteViewWasResized:self];
    } else {
        if (recognizer.state == UIGestureRecognizerStateBegan) {
            self.initialPosition = self.center;
        }
        
        self.center = CGPointMake(self.initialPosition.x + self.panTranslation.x, self.initialPosition.y + self.panTranslation.y);
    }
}

- (void)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    // When pinching is detected simultaneously with panning, the pinch gesture does not end
    // until all fingers are lifted. In other words, the pinch gesture continues even if only
    // a single touch is detected. So we need to detect transitions from panning to pincing
    // manually.
    
    if (recognizer.numberOfTouches > 1) {
        if (self.lastNumberOfTouches == 1 || recognizer.state == UIGestureRecognizerStateBegan) {
            
            // Transitioning into pinching state, so update self.initialFrame and self.initialTouchRect
            self.initialFrame = self.frame;
            self.initialTouchRect = [PPPElementView touchRectFromGestureRecognizer:recognizer];
            
            // For performance reasons, hide the text view while pinching.
            _textLabel.hidden = YES;
        }
        
        CGRect touchRect = [PPPElementView touchRectFromGestureRecognizer:recognizer];
        CGFloat dx = CGRectGetMidX(touchRect) - CGRectGetMidX(self.initialTouchRect);
        CGFloat dy = CGRectGetMidY(touchRect) - CGRectGetMidY(self.initialTouchRect);
        CGFloat dw = CGRectGetWidth(touchRect) - CGRectGetWidth(self.initialTouchRect);
        CGFloat dh = CGRectGetHeight(touchRect) - CGRectGetHeight(self.initialTouchRect);
        
        // A pinch is performed with multiple touches. These touches are bounded by a rectangle
        // called the touch rect. The callout is resized linearly with respect to changes in the
        // width and height of the touch rect.
        
        CGRect newFrame = (CGRect){
            CGRectGetMinX(self.initialFrame) + dx - dw / 2 + fmin((dw + self.initialFrame.size.width) / 2, 0),
            CGRectGetMinY(self.initialFrame) + dy - dh / 2 + fmin((dh + self.initialFrame.size.height) / 2, 0),
            CGRectGetWidth(self.initialFrame) + fmax(dw, -self.initialFrame.size.width),
            CGRectGetHeight(self.initialFrame) + fmax(dh, -self.initialFrame.size.height)
        };
        
        self.frame = newFrame;
            
    } else if (self.lastNumberOfTouches > 1) {
        
        // Transitioning from pinching to panning, so update self.initialPosition. We effectively "reset"
        // the pan recognizer's translationInView property by subtracting it from self.initialPosition.
        CGPoint translation = self.panTranslation;
        self.initialPosition = CGPointMake(self.center.x - translation.x, self.center.y - translation.y);
        
        [self didStopPinching];
    }

    self.lastNumberOfTouches = recognizer.numberOfTouches;
}

- (void)handlePress:(UILongPressGestureRecognizer *)recognizer {
    // TODO: Add animation signalling long press: a thin-bordered circle springs from the touch point,
    // fading in and bouncing.
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        // User long-pressed note. Create a new tail branch.
        PPPNoteBranchView *branchView = [self addTailWithPosition:[recognizer locationInView:recognizer.view]];
        [self.delegate noteView:self didAddTailInResponseToPress:branchView];
    }
}

@end
