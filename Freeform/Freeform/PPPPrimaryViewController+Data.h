//
//  PPPPrimaryViewController+Data.h
//  Freeform
//
//  Created by Alaric Holloway on 11/30/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPPrimaryViewController.h"
#import "PPPNoteView.h"

@interface PPPPrimaryViewController (Data) <PPPNoteViewDelegate>

- (void)saveManagedObjectContext;

@end
