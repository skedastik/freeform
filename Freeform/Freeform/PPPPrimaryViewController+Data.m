//
//  PPPPrimaryViewController+Data.m
//  Freeform
//
//  Created by Alaric Holloway on 11/30/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPAppDelegate.h"
#import "PPPPrimaryViewController+Data.h"
#import "PPPPrimaryViewController+ElementManagement.h"

@implementation PPPPrimaryViewController (Data)

#pragma mark - Note view delegate methods

- (void)noteViewWasResized:(PPPNoteView *)noteView {
    NSLog(@"Note view was resized.");
}

- (void)noteView:(PPPNoteView *)noteView tailDidMove:(PPPNoteBranchView *)branchView {
    NSLog(@"Note tail did move.");
}

- (void)noteView:(PPPNoteView *)noteView didAddTailInResponseToPress:(PPPNoteBranchView *)branchView {
    NSLog(@"Note did add tail in response to press.");
}

- (void)noteView:(PPPNoteView *)noteView willRemoveBranchInResponseToFlick:(PPPNoteBranchView *)branchView {
    NSLog(@"Note did will remove branch in response to flick.");
}

- (void)noteView:(PPPNoteView *)noteView willAbsorbBranch:(PPPNoteBranchView *)branchView {
    NSLog(@"Note did will absorb branch.");
}

#pragma mark - Context and fetched results controller

- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSManagedObjectContext *childContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [childContext setParentContext:self.ownerSpace.managedObjectContext];
    _managedObjectContext = childContext;
    
    NSUndoManager *undoManager = [[NSUndoManager alloc] init];
    _managedObjectContext.undoManager = undoManager;
    [_managedObjectContext.undoManager disableUndoRegistration];
    
    return _managedObjectContext;
}

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"PPPElement" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"space == %@", self.ownerSpace];
    fetchRequest.predicate = predicate;
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"zIndex" ascending:YES];
    fetchRequest.sortDescriptors = [NSArray arrayWithObject:sort];
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    _fetchedResultsController = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}

- (void)saveManagedObjectContext {
    NSError *error = nil;
    [self.managedObjectContext save:&error];
    [PPPAppDelegate handleContextSaveError:error context:self.managedObjectContext inController:self];
}

#pragma mark - Fetched results controller delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    if (!self.userDrivenUpdates) {
        switch(type) {
            case NSFetchedResultsChangeInsert:
                NSLog(@"   FRC -> Insert (index: %u)", indexPath.row);
                [self insertElement:anObject];
                break;
                
            case NSFetchedResultsChangeDelete:
                NSLog(@"   FRC -> Delete (index: %u)", indexPath.row);
                [self removeElement:anObject];
                break;
                
            case NSFetchedResultsChangeUpdate:
                NSLog(@"   FRC -> Update (index: %u)", indexPath.row);
                [self updateElement:anObject];
                break;
                
            case NSFetchedResultsChangeMove:
                // NSFetchedResultsController doesn't send a separate Update notification if an object
                // has both moved and updated--only a Move notification is sent.
                NSLog(@"   FRC -> Move (%u -> %u)", indexPath.row, newIndexPath.row);
                [self updateElement:anObject withNewZIndex:newIndexPath.row];
                break;
        }
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self endUpdates];
}

@end
