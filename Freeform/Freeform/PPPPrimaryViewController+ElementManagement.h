//
//  PPPPrimaryViewController+ElementManagement.h
//  Freeform
//
//  Created by Alaric Holloway on 11/30/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//
//  Methods for managing both user-driven and model-driven insertions,
//  deletions, and updates of Freeform elements. In other words, these
//  methods respond to model updates and user actions.
//
//  IMPLEMENTATION
//
//  All Freeform elements have a unique z-index attribute that determines
//  their ordering in the workspace view. This index is used to identify
//  each element and map it to its corresponding view (much in the way
//  that a fetched results controller maps table view rows to model
//  objects via index paths). In fact, a fetched results controller is
//  such a good fit that it's exactly what we use. The 'row' index
//  corresponds directly to our elements' 'zIndex' attributes. In this
//  way, a fetched results controller enables us to detect insertions,
//  deletions, updates, and "moves" (changes in z-index) automatically.
//

#import "PPPPrimaryViewController.h"

@interface PPPPrimaryViewController (ElementManagement)

#pragma mark - User-driven changes

- (void)beginUserDrivenUpdates;
- (void)endUserDrivenUpdates;

- (void)selectElement:(PPPElement *)element;
- (void)deselectElement:(PPPElement *)element;
- (void)deselectAllElements;
- (void)toggleSelectionOfElement:(PPPElement *)element deselectingOthers:(BOOL)deselectingOthers;

#pragma mark - User-initiated changes

- (void)deleteSelectedElements;

#pragma mark - Model-driven changes

- (void)beginUpdates;
- (void)endUpdates;

- (void)updateElement:(PPPElement *)element;
- (void)updateElement:(PPPElement *)element withNewZIndex:(NSUInteger)newZIndex;
- (void)insertElement:(PPPElement *)element;
- (void)insertElements:(NSArray *)elements;
- (void)removeElement:(PPPElement *)element;
- (void)removeElements:(NSArray *)elements;

@end
