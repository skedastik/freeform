//
//  PPPPrimaryViewController+ElementManagement.m
//  Freeform
//
//  Created by Alaric Holloway on 11/30/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPPrimaryViewController+ElementManagement.h"
#import "PPPPrimaryViewController+Data.h"
#import "PPPPrimaryViewController+Overlay.h"
#import "PPPElement.h"
#import "PPPNote.h"
#import "PPPNoteView.h"
#import "PPPImage.h"
#import "PPPMath.h"

@implementation PPPPrimaryViewController (ElementManagement)

#pragma mark - User-driven changes

// These methods are called as a result of user action. They directly update the
// managed object context _and_ corresponding objects. Any user-driven actions
// that result in changes to the model are bracketed with calls to
// 'beginUserDrivenUpdates' and 'endUserDrivenUpdates'. This automatically saves
// the context and makes those actions undoable.

- (void)beginUserDrivenUpdates {
    NSLog(@"Beginning user-driven updates... (nest-level: %u)", self.userDrivenUpdates + 1);
    self.userDrivenUpdates++;
    [self.managedObjectContext.undoManager beginUndoGrouping];
}

- (void)endUserDrivenUpdates {
    [self saveManagedObjectContext];
    NSLog(@"End of user-driven updates... (nest-level: %u)", self.userDrivenUpdates - 1);
    if (self.userDrivenUpdates == 1 && self.updatedElements.count == 0) {
        NSLog(@" ");
    }
    [self.managedObjectContext.undoManager endUndoGrouping];
    self.userDrivenUpdates--;
    [self updateUndoAndRedoButtons];
}

- (void)selectElement:(PPPElement *)element {
    [self beginUserDrivenUpdates];
    
    if (![self.selectedElements containsObject:element]) {
        [self.view elementViewAtZIndex:element.zIndex].selected = YES;
        element.selected = YES;
        [self.selectedElements addObject:element];
        NSLog(@"   Selecting element at z-index: %u (# selected: %u)", element.zIndex, self.selectedElements.count);
    }
    
    [self endUserDrivenUpdates];
}

- (void)deselectElement:(PPPElement *)element {
    [self beginUserDrivenUpdates];
    
    if ([self.selectedElements containsObject:element]) {
        [self.view elementViewAtZIndex:element.zIndex].selected = NO;
        element.selected = NO;
        [self.selectedElements removeObject:element];
        NSLog(@"   Deselecting element at z-index: %u (# selected: %u)", element.zIndex, self.selectedElements.count);
    }
    
    [self endUserDrivenUpdates];
}

- (void)deselectAllElements {
    [self beginUserDrivenUpdates];
    
    [[self.selectedElements copy] enumerateObjectsUsingBlock:^(PPPElement *element, NSUInteger idx, BOOL *stop) {
        [self deselectElement:element];
    }];
    
    [self endUserDrivenUpdates];
}

- (void)toggleSelectionOfElement:(PPPElement *)element deselectingOthers:(BOOL)deselectingOthers {
    [self beginUserDrivenUpdates];
    
    BOOL doSelect = !element.selected;
    
    if (deselectingOthers) {
        [self deselectAllElements];
    }
    
    if (doSelect) {
        [self selectElement:element];
    } else {
        [self deselectElement:element];
    }
    
    [self endUserDrivenUpdates];
}

#pragma mark - User-initiated changes

// These methods are called as a result of the user's actions and result in
// changes to the managed object context. They have no direct effect on views.

- (void)deleteSelectedElements {
    [self.managedObjectContext.undoManager beginUndoGrouping];
    
    [self.selectedElements enumerateObjectsUsingBlock:^(PPPElement *element, NSUInteger idx, BOOL *stop) {
        [self.managedObjectContext deleteObject:element];
    }];
    
    [self saveManagedObjectContext];
    [self.managedObjectContext.undoManager endUndoGrouping];
}

#pragma mark - Model-driven changes

// These methods are called by the fetched results controller as a result of
// changes to the managed object context. Each update is bracketed by a call
// to 'beginUpdates' and 'endUpdates'. Views are directly updated at the end
// of updates (via 'endUpdates').

- (void)beginUpdates {
    if (!self.userDrivenUpdates) {
        NSLog(@"Beginning model-driven updates...");
    }
}

- (void)endUpdates {
    if (!self.userDrivenUpdates) {
        if (self.deletedElements.count > 0 && self.insertedElements.count > 0) {
            // Currently, no action can result in simultaneous insertions and deletions
            // within the same update block. It is a programmer error if this occurs.
            @throw([NSException exceptionWithName:@"Simultaneous insertion and deletion in single update block."
                                           reason:@"Simultaneous insertion and deletion should never occur within a single update block."
                                         userInfo:@{@"insertion count" : [NSNumber numberWithUnsignedInteger:self.insertedElements.count],
                                                    @"deletion count"  : [NSNumber numberWithUnsignedInteger:self.deletedElements.count]}]);
        }
        
        // Insertions and deletions automatically trigger re-ordering of view z-indices, rendering
        // the move phase unecessary. Moves are processed only during pure updates.
        BOOL movePhase = (self.insertedElements.count == 0 && self.deletedElements.count == 0) ? YES : NO;
        
        // Order is crucial here!
        [self processInsertions];
        if (movePhase) { [self processMoves]; }
        [self processUpdates];
        [self processDeletions];
        
        self.insertedElements = [[NSMutableArray alloc] init];
        self.deletedElements = [[NSMutableArray alloc] init];
        self.updatedElements = [[NSMutableArray alloc] init];
        self.movedElements = [[NSMutableArray alloc] init];
        
        NSLog(@"End of model-driven updates.");
        NSLog(@" ");
    }
}

- (void)insertElement:(PPPElement *)element {
    if (![self.insertedElements containsObject:element]) {
        [self.insertedElements addObject:element];
        
        if (element.selected) {
            [self.selectedElements addObject:element];
        }
        
        self.elementCount++;
    }
}

- (void)insertElements:(NSArray *)elements {
    [elements enumerateObjectsUsingBlock:^(PPPElement *element, NSUInteger idx, BOOL *stop) {
        [self insertElement:element];
    }];
}

- (void)removeElement:(PPPElement *)element {
    if (![self.deletedElements containsObject:element]) {
        [self.deletedElements addObject:element];
        
        if (element.selected) {
            [self.selectedElements removeObject:element];
        }
        
        self.elementCount--;
    }
}

- (void)removeElements:(NSArray *)elements {
    [elements enumerateObjectsUsingBlock:^(PPPElement *element, NSUInteger idx, BOOL *stop) {
        [self removeElement:element];
    }];
}

- (void)updateElement:(PPPElement *)element {
    if (![self.updatedElements containsObject:element]) {
        [self.updatedElements addObject:element];
    }
    
    // TODO: Update self.selectedElements based on element's selected attribute
}

- (void)updateElement:(PPPElement *)element withNewZIndex:(NSUInteger)newZIndex {
    [self updateElement:element];
    [self.movedElements addObject:@[[NSNumber numberWithUnsignedInteger:element.zIndex],
                                    [NSNumber numberWithUnsignedInteger:newZIndex]]];
}

- (void)processInsertions {
    if (self.insertedElements.count > 0) {
        NSLog(@"   Beginning insertions...");
        
        // Sort inserted elements by ascending z-index to avoid out-of-range accesses
        NSArray *sortedElements = [PPPPrimaryViewController elementsSortedByZIndex:self.insertedElements ascending:YES];
        
        NSMutableArray *insertedNotes = [[NSMutableArray alloc] init];
        
        // Generate views for inserted elements...
        [sortedElements enumerateObjectsUsingBlock:^(PPPElement *element, NSUInteger idx, BOOL *stop) {
            NSUInteger zIndex = element.zIndex;
            PPPElementView *newElementView;
            
            if (zIndex > self.elementCount) {
                // It is a programmer error if an element is inserted with z-index > elementCount.
                // New views can be inserted no higher than directly above the top-most view.
                @throw([NSException exceptionWithName:@"Element z-index out of range."
                                               reason:@"An element was inserted with z-index out of range."
                                             userInfo:@{@"z-index"       : [NSNumber numberWithUnsignedInteger:zIndex],
                                                        @"element count" : [NSNumber numberWithUnsignedInteger:self.elementCount]}]);
            }
            
            if ([element isKindOfClass:[PPPNote class]]) {
                NSLog(@"      Inserting note at zIndex: %u", element.zIndex);
                
                PPPNoteView *noteView = [self noteViewFromNote:(PPPNote *)element];
                noteView.delegate = (id <PPPNoteViewDelegate>)self;
                newElementView = noteView;
                
                // Collect inserted note elements in another array
                [insertedNotes addObject:element];
            }
            
            // Add the element view to the Freeform workspace
            [self.view insertElementView:newElementView atIndex:zIndex];
        }];
        
        // Traverse array of inserted note elements one more time to link note views
        [insertedNotes enumerateObjectsUsingBlock:^(PPPNote *note, NSUInteger idx, BOOL *stop) {
            PPPNoteView *noteView = (PPPNoteView *)[self.view elementViewAtZIndex:note.zIndex];
            [self updateNoteViewLinks:noteView withNote:note];
        }];
        
        NSLog(@"   End of insertions (total elements: %u, selected: %u)", self.fetchedResultsController.fetchedObjects.count, self.selectedElements.count);
    }
}

- (void)processDeletions {
    if (self.deletedElements.count > 0) {
        NSLog(@"   Beginning deletions...");
        
        // Sort deleted elements by descending z-index to avoid out-of-range accesses
        NSArray *sortedElements = [PPPPrimaryViewController elementsSortedByZIndex:self.deletedElements ascending:NO];
        
        [sortedElements enumerateObjectsUsingBlock:^(PPPElement *element, NSUInteger idx, BOOL *stop) {
            NSLog(@"      Removing element at zIndex: %u", element.zIndex);
            
            // Remove deleted element's corresponding view from its superview
            [[self.view elementViewAtZIndex:element.zIndex] removeFromSuperview];
        }];
        
        NSLog(@"   End of deletions (total elements: %u, selections: %u)", self.fetchedResultsController.fetchedObjects.count, self.selectedElements.count);
    }
}

- (void)processUpdates {
    if (self.updatedElements.count > 0) {
        NSLog(@"   Beginning updates...");
        
        [self.updatedElements enumerateObjectsUsingBlock:^(PPPElement *element, NSUInteger idx, BOOL *stop) {
            if (element.class == [PPPNote class]) {
                PPPNoteView *noteView = (PPPNoteView *)[self.view elementViewAtZIndex:element.zIndex];
                PPPNote *note = (PPPNote *)element;
                [self updateNoteView:noteView withNote:note];
                [self updateNoteViewLinks:noteView withNote:note];
            }
        }];
        
        NSLog(@"   End of updates. (# selected: %u)", self.selectedElements.count);
    }
}

- (void)processMoves {
    if (self.movedElements.count > 0) {
        NSLog(@"   Beginning moves...");
        
        [self.movedElements enumerateObjectsUsingBlock:^(NSArray *movePair, NSUInteger idx, BOOL *stop) {
            NSUInteger fromIndex = [(NSNumber *)movePair[0] unsignedIntegerValue];
            NSUInteger toIndex = [(NSNumber *)movePair[1] unsignedIntegerValue];
            [self.view exchangeElementViewAtIndex:fromIndex withElementViewAtIndex:toIndex];
            
            NSLog(@"      %u -> %u", fromIndex, toIndex);
        }];
        
        NSLog(@"   End of moves.");
    }
}

#pragma mark - Helper functions

+ (NSArray *)elementsSortedByZIndex:(NSArray *)elements ascending:(BOOL)ascending {
    return [elements sortedArrayUsingComparator:^NSComparisonResult(PPPElement *element1, PPPElement *element2)
    {
        if (element1.zIndex > element2.zIndex) {
            return ascending ? NSOrderedDescending : NSOrderedAscending;
        }

        if (element1.zIndex < element2.zIndex) {
            return ascending ? NSOrderedAscending : NSOrderedDescending;
        }

        return NSOrderedSame;
    }];
}

#pragma mark - Note view factory

// Returns a new PPPNoteView configured with data from the given PPPNote.
// Views for tails are automatically generated, but not for links.
- (PPPNoteView *)noteViewFromNote:(PPPNote *)note {
    PPPNoteView *noteView = [[PPPNoteView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self updateNoteView:noteView withNote:note];
    
    return noteView;
}

// Configures an existing note view with attributes from given note.
// Views for tails are automatically generated, but not for links.
// Use 'updateNoteViewLinks' to generate views for links.
- (void)updateNoteView:(PPPNoteView *)noteView withNote:(PPPNote *)note {
    CGPoint center = CGPointFromString(note.center);
    
    NSLog(@"      Configuring note with z-index: %u (text: \"%@...\")", note.zIndex, [note.text substringToIndex:5]);
    
    noteView.frame = CGRectMakeCentered(center.x, center.y, note.width, note.height);
    noteView.text = note.text;
    noteView.swatchIndex = note.swatchIndex;
    noteView.selected = note.selected;
    
    // Update tail branch views
    if (!(note.tails.count == 0 && noteView.numberOfTails == 0)) {
        NSArray *tailPositions = [note.tails valueForKey:@"position"];
        [noteView updateTailsWithArray:tailPositions];
    }
}

// Update a note view's out-link branch views using data from given note.
- (void)updateNoteViewLinks:(PPPNoteView *)noteView withNote:(PPPNote *)note {
    if (!(note.outLinks.count == 0 && noteView.numberOfOutLinks == 0)) {
        NSSet *outLinkNotes = note.outLinks;
        NSMutableArray *outLinkNoteViews = [[NSMutableArray alloc] init];
        
        // First we need to generate an array of PPPNoteView objects to which this note has out-links.
        [outLinkNotes enumerateObjectsUsingBlock:^(PPPNote *note, BOOL *stop) {
            [outLinkNoteViews addObject:[self.view elementViewAtZIndex:note.zIndex]];
        }];
        
        [noteView updateOutLinksWithArray:outLinkNoteViews];
    }
}

@end
