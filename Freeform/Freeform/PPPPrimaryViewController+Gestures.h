//
//  PPPPrimaryViewController+Gestures.h
//  Freeform
//
//  Created by Alaric Holloway on 11/30/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPPrimaryViewController.h"


@interface PPPPrimaryViewController (Gestures) <UIGestureRecognizerDelegate>

- (void)initGestures;
- (void)toggleGestureMode;

@end
