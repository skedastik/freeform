//
//  PPPPrimaryViewController+Gestures.m
//  Freeform
//
//  Created by Alaric Holloway on 11/30/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPPrimaryViewController+Gestures.h"
#import "PPPPrimaryViewController+Overlay.h"
#import "PPPPrimaryViewController+ElementManagement.h"
#import "PPPElement.h"

// Minimum amount of time before long press gesture is triggered in seconds
const CFTimeInterval PPPMinimumPressDuration = 0.4;

typedef enum {
    PPPRecognizerTypeTap,
    PPPRecognizerTypeDoubleTap,
    PPPRecognizerTypePan,
    PPPRecognizerTypePinch,
    PPPRecognizerTypePress
} PPPRecognizerType;


@implementation PPPPrimaryViewController (Gestures)

- (PPPDynamicView *)activeDynamicView {
    return _activeDynamicView;
}

- (void)setActiveDynamicView:(PPPDynamicView *)dynamicView {
    if (self.activeDynamicView) {
        [self.activeDynamicView concludeInteraction];
    }
    
    _activeDynamicView = dynamicView;
}

#pragma mark - Gesture mode

- (void)setGestureMode:(PPPGestureMode)newGestureMode {
    if (_gestureMode != newGestureMode) {
        [self concludeGestureMode:_gestureMode];
        [self beginGestureMode:newGestureMode];
        
        _gestureMode = newGestureMode;
        
        [self.view updateGestureModeButtonWithMode:self.gestureMode];
    }
}

- (PPPGestureMode)gestureMode {
    return _gestureMode;
}

- (void)beginGestureMode:(PPPGestureMode)gestureMode {
    switch (gestureMode) {
        case PPPGestureModeMultiselect:
            // Nothing to do.
            break;
            
        case PPPGestureModeSelection:
            // Nothing to do.
            break;
            
        case PPPGestureModeManipulation:
            // Nothing to do.
            break;
    }
}

- (void)concludeGestureMode:(PPPGestureMode)gestureMode {
    switch (gestureMode) {
        case PPPGestureModeMultiselect:
            // Nothing to do.
            break;
            
        case PPPGestureModeSelection:
            // Nothing to do.
            break;
            
        case PPPGestureModeManipulation:
            // Nothing to do.
            break;
    }
}

- (void)toggleGestureMode {
    if (self.gestureMode == PPPGestureModeManipulation) {
        self.gestureMode = PPPGestureModeSelection;
    } else {
        self.gestureMode = PPPGestureModeManipulation;
    }
}

#pragma mark - Dynamic gesture handling

// Freeform responds to gestures differently based on the current
// gesture mode and the current number of selected elements. See
// interaction matrix document for exact behavioral specification.

- (void)handleGestureRecognizer:(UIGestureRecognizer *)recognizer ofType:(PPPRecognizerType)recognizerType {
    if (self.gestureMode == PPPGestureModeManipulation) {
        [self handleManipulationGestureRecognizer:recognizer ofType:recognizerType];
    } else {
        [self handleSelectionGestureRecognizer:recognizer ofType:recognizerType];
    }
}

- (void)handleManipulationGestureRecognizer:(UIGestureRecognizer *)recognizer ofType:(PPPRecognizerType)recognizerType {
    switch (recognizerType) {
        case PPPRecognizerTypePan:
            break;
            
        case PPPRecognizerTypeTap:
            [self handleManipulationTap:(UITapGestureRecognizer *)recognizer];
            break;
            
        case PPPRecognizerTypeDoubleTap:
            break;
            
        case PPPRecognizerTypePress:
            break;
            
        case PPPRecognizerTypePinch:
            break;
    }
}

- (void)handleSelectionGestureRecognizer:(UIGestureRecognizer *)recognizer ofType:(PPPRecognizerType)recognizerType {
    // TODO:
}

- (void)handleManipulationTap:(UITapGestureRecognizer *)recognizer {
    PPPElementView *elementView = (PPPElementView *)[self.view hitTestForViewClass:[PPPElementView class] withRecognizer:recognizer];
    
    if (elementView) {
        if (self.selectedElements.count > 1) {
            // TODO: Handle manipulation tap for multiple selections
        } else {
            NSUInteger zIndex = [self.view zIndexOfElementView:elementView];
            PPPElement *element = [self.fetchedResultsController.fetchedObjects objectAtIndex:zIndex];
            [self toggleSelectionOfElement:element deselectingOthers:YES];
        }
    } else if (self.selectedElements.count > 0) {
        // User tapped empty space, so clear selections
        [self deselectAllElements];
    }
}

#pragma mark - Gesture recognizers

- (void)initGestures {
    self.gestureCount = 0;
    self.activeDynamicView = nil;
    
    if (!self.tapRecognizer) {
        self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
        self.tapRecognizer.numberOfTapsRequired = 1;
        self.tapRecognizer.numberOfTouchesRequired = 1;
        self.tapRecognizer.delegate = self;
        [self.view addGestureRecognizer:self.tapRecognizer];
        
        self.doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didDoubleTap:)];
        self.doubleTapRecognizer.numberOfTapsRequired = 2;
        self.doubleTapRecognizer.numberOfTouchesRequired = 1;
        self.doubleTapRecognizer.delegate = self;
        [self.view addGestureRecognizer:self.doubleTapRecognizer];
        
        self.pressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(didPress:)];
        self.pressRecognizer.minimumPressDuration = PPPMinimumPressDuration;
        [self.view addGestureRecognizer:self.pressRecognizer];
        
        self.pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(didPinch:)];
        self.pinchRecognizer.delegate = self;
        [self.view addGestureRecognizer:self.pinchRecognizer];
        
        self.panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didPan:)];
        [self.view addGestureRecognizer:self.panRecognizer];
    }
}

// Check if simultaneous pinch, pan, and rotate gestures are done. If so, invoke gesturesDidEnd
- (void)trackGestureRecognizer:(UIGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.gestureCount++;
    } else if ((recognizer.state == UIGestureRecognizerStateEnded ||
                recognizer.state == UIGestureRecognizerStateCancelled ||
                recognizer.state == UIGestureRecognizerStateFailed)
               &&
               --self.gestureCount == 0)
    {
        [self gesturesDidEnd];
    }
}

- (void)gesturesDidEnd {
    self.activeDynamicView = nil;
}

- (void)didTap:(UITapGestureRecognizer *)recognizer {
    [self handleGestureRecognizer:recognizer ofType:PPPRecognizerTypeTap];
}

- (void)didDoubleTap:(UITapGestureRecognizer *)recognizer {
    [self handleGestureRecognizer:recognizer ofType:PPPRecognizerTypeDoubleTap];
}

- (void)didPan:(UIPanGestureRecognizer *)recognizer {
    [self trackGestureRecognizer:recognizer];
    [self handleGestureRecognizer:recognizer ofType:PPPRecognizerTypePan];
}

- (void)didPinch:(UIPinchGestureRecognizer *)recognizer {
    [self trackGestureRecognizer:recognizer];
    [self handleGestureRecognizer:recognizer ofType:PPPRecognizerTypePinch];
}

- (void)didPress:(UILongPressGestureRecognizer *)recognizer {
    [self trackGestureRecognizer:recognizer];
    [self handleGestureRecognizer:recognizer ofType:PPPRecognizerTypePress];
}

#pragma mark - Gesture recognizer delegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    // Long press and pinch should not be recognized simultaneously.
    // Tap and double tap should not be recognized simultaneously.
    return (!(gestureRecognizer == self.pinchRecognizer && otherGestureRecognizer == self.pressRecognizer) &&
            !(gestureRecognizer == self.pressRecognizer && otherGestureRecognizer == self.pinchRecognizer) &&
            !(gestureRecognizer == self.tapRecognizer && otherGestureRecognizer == self.doubleTapRecognizer) &&
            !(gestureRecognizer == self.doubleTapRecognizer && otherGestureRecognizer == self.tapRecognizer));
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    // Recognizers shouldn't respond to taps or double taps on UIButtons
    return !((gestureRecognizer == self.tapRecognizer || gestureRecognizer == self.doubleTapRecognizer) && [touch.view isKindOfClass:[UIButton class]]);
}

@end
