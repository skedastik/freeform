//
//  PPPPrimaryViewController+Overlay.h
//  Freeform
//
//  Created by Alaric Holloway on 11/30/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPPrimaryViewController.h"

@interface PPPPrimaryViewController (Overlay)

#pragma mark - Button actions

- (IBAction)gestureModeButtonWasTapped:(id)sender;
- (IBAction)undoButtonWasTapped:(id)sender;
- (IBAction)redoButtonWasTapped:(id)sender;
- (IBAction)expandButtonWasTapped:(id)sender;
- (IBAction)newImageButtonWasTapped:(id)sender;
- (IBAction)newNoteButtonWasTapped:(id)sender;
- (IBAction)pinningButtonWasTapped:(id)sender;
- (IBAction)deleteElementButtonWasTapped:(id)sender;

#pragma mark - Misc

- (void)updateUndoAndRedoButtons;

@end
