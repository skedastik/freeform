//
//  PPPPrimaryViewController+Overlay.m
//  Freeform
//
//  Created by Alaric Holloway on 11/30/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPPrimaryViewController+Overlay.h"
#import "PPPPrimaryViewController+Gestures.h"
#import "PPPPrimaryViewController+ElementManagement.h"

@implementation PPPPrimaryViewController (Overlay)

#pragma mark - Overlay button actions

- (void)gestureModeButtonWasTapped:(id)sender {
    [self toggleGestureMode];
}

- (IBAction)undoButtonWasTapped:(id)sender {
    NSLog(@"Beginning undo...");
    NSLog(@" ");
    [self.managedObjectContext.undoManager undo];
    NSLog(@"End of undo.");
    [self updateUndoAndRedoButtons];
}

- (IBAction)redoButtonWasTapped:(id)sender {
    NSLog(@"Beginning redo...");
    NSLog(@" ");
    [self.managedObjectContext.undoManager redo];
    NSLog(@"End of redo.");
    [self updateUndoAndRedoButtons];
}

- (IBAction)expandButtonWasTapped:(id)sender {
}

- (IBAction)newImageButtonWasTapped:(id)sender {
}

- (IBAction)newNoteButtonWasTapped:(id)sender {
}

- (IBAction)pinningButtonWasTapped:(id)sender {
}

- (IBAction)deleteElementButtonWasTapped:(id)sender {
    [self deleteSelectedElements];
}

#pragma mark - Misc

- (void)updateUndoAndRedoButtons {
    BOOL enableUndo = [self.managedObjectContext.undoManager canUndo];
    BOOL enableRedo = [self.managedObjectContext.undoManager canRedo];
    [self.view enableUndoButton:enableUndo andRedoButton:enableRedo];
}

@end
