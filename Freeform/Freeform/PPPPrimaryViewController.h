//
//  PPPPrimaryViewController.h
//  Freeform
//
//  Created by Alaric Holloway on 11/26/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//
//  The primary view is the Freeform workspace view where the user will be
//  doing all of his work. It supports zoom and pan via a UIScrollView.
//

#import <UIKit/UIKit.h>
#import "PPPGestureMode.h"
#import "PPPWorkspaceView.h"
#import "PPPSpace.h"
#import "PPPDynamicView.h"


@interface PPPPrimaryViewController : UIViewController <NSFetchedResultsControllerDelegate>
{
    // Expose these ivars for various categories
    PPPGestureMode _gestureMode;
    __weak PPPDynamicView *_activeDynamicView;
    NSManagedObjectContext *_managedObjectContext;
    NSFetchedResultsController *_fetchedResultsController;
}

@property (nonatomic, retain) PPPWorkspaceView *view;

#pragma mark - State

@property (nonatomic) NSUInteger elementCount;
@property (nonatomic, strong) NSMutableArray *selectedElements;
@property (nonatomic, strong) NSMutableArray *insertedElements;
@property (nonatomic, strong) NSMutableArray *deletedElements;
@property (nonatomic, strong) NSMutableArray *updatedElements;
@property (nonatomic, strong) NSMutableArray *movedElements;

// This property is > 0 if user-driven updates are in progress
@property (nonatomic) NSUInteger userDrivenUpdates;

#pragma mark - Data

@property (nonatomic, strong) PPPSpace *ownerSpace;
@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong, readonly) NSFetchedResultsController *fetchedResultsController;

#pragma mark - Gestures

// gestureMode defines how Freeform will respond to gestures.
// See interaction matrix document for exact behavior.
@property (nonatomic) PPPGestureMode gestureMode;

// The element view currently being manipulated by gestures (otherwise nil)
@property (nonatomic, weak) PPPDynamicView *activeDynamicView;

@property (nonatomic) NSUInteger gestureCount;

@property (nonatomic, strong) UIPanGestureRecognizer *panRecognizer;
@property (nonatomic, strong) UIPinchGestureRecognizer *pinchRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer *doubleTapRecognizer;
@property (nonatomic, strong) UILongPressGestureRecognizer *pressRecognizer;

@end
