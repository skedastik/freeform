//
//  PPPPrimaryViewController.m
//  Freeform
//
//  Created by Alaric Holloway on 11/26/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPAppDelegate.h"
#import "PPPPrimaryViewController.h"
#import "PPPPrimaryViewController+Gestures.h"
#import "PPPPrimaryViewController+Data.h"
#import "PPPPrimaryViewController+Overlay.h"
#import "PPPPrimaryViewController+ElementManagement.h"
#import "PPPNoteView.h"


@implementation PPPPrimaryViewController

@dynamic gestureMode;
@dynamic activeDynamicView;
@dynamic managedObjectContext;
@dynamic fetchedResultsController;

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.selectedElements = [[NSMutableArray alloc] init];
        self.insertedElements = [[NSMutableArray alloc] init];
        self.deletedElements = [[NSMutableArray alloc] init];
        self.updatedElements = [[NSMutableArray alloc] init];
        self.movedElements = [[NSMutableArray alloc] init];
        
        self.userDrivenUpdates = 0;
        self.elementCount = 0;
        _gestureMode = PPPGestureModeManipulation;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view initOverlayImages];
    [self.view setTheme:self.ownerSpace.theme];
    [self updateUndoAndRedoButtons];
    [self initGestures];
    
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    [PPPAppDelegate handlePerformFetchError:error inController:self];
    
    // Begin undo registration
    [self.managedObjectContext.undoManager enableUndoRegistration];
    
    // Display fetched objects
    [self beginUpdates];
    [self insertElements:self.fetchedResultsController.fetchedObjects];
    [self endUpdates];
}

- (void)viewWillAppear:(BOOL)animated {
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:animated ? UIStatusBarAnimationSlide : UIStatusBarAnimationNone];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:animated ? UIStatusBarAnimationSlide : UIStatusBarAnimationNone];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self.view.scrollView updateMinMaxZoomScale];
}

@end
