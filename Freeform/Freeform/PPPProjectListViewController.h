//
//  PPPProjectsViewController.h
//  Freeform
//
//  Created by Alaric Holloway on 11/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPPEditableListViewController.h"

@interface PPPProjectListViewController : PPPEditableListViewController <NSFetchedResultsControllerDelegate, UITextFieldDelegate, UIAlertViewDelegate, UIActionSheetDelegate>

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
