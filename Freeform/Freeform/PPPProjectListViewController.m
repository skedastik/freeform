//
//  PPPProjectsViewController.m
//  Freeform
//
//  Created by Alaric Holloway on 11/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPAppDelegate.h"
#import "PPPProjectListViewController.h"
#import "PPPSpaceListViewController.h"
#import "PPPTextFieldCell.h"
#import "PPPProject.h"

@interface PPPProjectListViewController ()

@end

@implementation PPPProjectListViewController {
    NSIndexPath *_indexPathForDeletion;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    
    if (self) {
        _indexPathForDeletion = nil;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }
    
    return self;
}

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"PPPProject" inManagedObjectContext:self.managedObjectContext];
    fetchRequest.entity = entity;
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"dateCreated" ascending:NO];
    fetchRequest.sortDescriptors = [NSArray arrayWithObject:sort];
    
    fetchRequest.fetchBatchSize = 20;
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil
                                                   cacheName:@"ProjectsCache"];
    self.fetchedResultsController = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
 
    self.title = NSLocalizedString(@"Projects", nil);
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didActivatePulldown {
    [super didActivatePulldown];
    self.tableView.scrollLockEnabled = YES;
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"New Project", nil)
                                                        message:NSLocalizedString(@"Name your project.", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                              otherButtonTitles:NSLocalizedString(@"Done", nil), nil];
    alertView.alertViewStyle = UIAlertViewStyleDefault;
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView textFieldAtIndex:0].placeholder = NSLocalizedString(@"Unnamed Project", nil);
    [alertView show];
}

#pragma mark - UIAlert and UIActionSheet delegates

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0 && _indexPathForDeletion) {
        // Delete the project marked by the user
        NSManagedObjectContext *context = self.managedObjectContext;
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:_indexPathForDeletion]];

        NSError *error = nil;
        [context save:&error];
        [PPPAppDelegate handleContextSaveError:error context:context inController:self];
        
        _indexPathForDeletion = nil;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex > 0) {
        NSManagedObjectContext *context = self.managedObjectContext;
        PPPProject *project = [NSEntityDescription insertNewObjectForEntityForName:@"PPPProject" inManagedObjectContext:context];
        
        NSString *text = [alertView textFieldAtIndex:0].text;
        project.name = [text isEqualToString:@""] ? NSLocalizedString(@"Unnamed Project", nil) : text;
        
        NSError *error = nil;
        [context save:&error];
        [PPPAppDelegate handleContextSaveError:error context:context inController:self];
    }
    
    self.tableView.scrollLockEnabled = NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger numRows = [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
    
    // Hide or show the edit button based on the number of rows in the table
    if (numRows > 0) {
        [self.navigationItem setRightBarButtonItem:self.editButtonItem animated:YES];
    } else {
        [self.navigationItem setRightBarButtonItem:nil animated:YES];
        
        // Disable edit mode if no rows exist
        [self setEditing:NO animated:YES];
    }
    
    [self updatePulldown:numRows];
    
    return numRows;
}

- (void)configureCell:(PPPTextFieldCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    PPPProject *project = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textField.text = project.name;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ProjectCell";
    PPPTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textField.placeholder = NSLocalizedString(@"Unnamed Project", nil);
    cell.textField.delegate = self;
    cell.textField.ownerCell = cell;
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    PPPProject *project = [self.fetchedResultsController objectAtIndexPath:indexPath];
    _indexPathForDeletion = indexPath;
    
    NSString *descriptionFormat = NSLocalizedString(@"Are you sure you want to delete the project named \"%@\"? Any space it contains will also be deleted.", "This string is used as a format string and must include a format specifier for the project name.");
    NSString *description = [NSString stringWithFormat:descriptionFormat, project.name];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:description
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"Delete Project", nil)
                                                    otherButtonTitles:nil];
    [actionSheet showInView:tableView];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    if ([[segue identifier] isEqualToString:@"ProjectsToSpaces"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        PPPProject *project = (PPPProject *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
        
        PPPSpaceListViewController *controller = (PPPSpaceListViewController *)[segue destinationViewController];
        controller.ownerProject = project;
    }
}

#pragma mark - UITextField delegate

- (void)textFieldDidEndEditing:(PPPTextField *)textField {
    // User has changed name of project. Validate the name string.
    if ([textField.text isEqualToString:@""]) {
        textField.text = textField.placeholder;
    }
    
    // Propagate change to managed object.
    NSIndexPath *indexPath = [self.tableView indexPathForCell:textField.ownerCell];
    PPPProject *project = [self.fetchedResultsController objectAtIndexPath:indexPath];
    project.name = textField.text;
    
    NSError *error = nil;
    [self.managedObjectContext save:&error];
    [PPPAppDelegate handleContextSaveError:error context:self.managedObjectContext inController:self];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return NO;
}

@end
