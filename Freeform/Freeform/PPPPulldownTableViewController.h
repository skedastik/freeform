//
//  PPPPulldownTableViewController.h
//  SlickTableView
//
//  Created by Alaric Holloway on 11/24/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//
//  A pulldown table view allows the user to scroll down past the topmost cell to activate
//  an action (e.g. inserting a new cell). This is a great way of reducing the visual
//  footprint of the GUI by harnessing gestures. A pulldown arrow is displayed in the
//  first row of the table view to signal the user to swipe down. Doing so reveals a
//  pulldown confirmation. If the user lifts his finger while the confirmation is fully
//  disclosed, the pulldown action is invoked.
//

#import <UIKit/UIKit.h>
#import "PPPScrollLockTableView.h"
#import "PPPFetchTableViewController.h"

@interface PPPPulldownTableViewController : PPPFetchTableViewController <UIAlertViewDelegate>

// Refine tableView property to more specific type.
@property (nonatomic,retain) PPPScrollLockTableView *tableView;

// Call this when the number of rows changes. Pulldown arrow is hidden or shown accordingly.
- (void)updatePulldown:(NSUInteger)numberOfRows;

// This method is called when the pulldown is activated. Override as desired.
- (void)didActivatePulldown;

@end
