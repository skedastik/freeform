//
//  PPPPulldownTableViewController.h
//  SlickTableView
//
//  Created by Alaric Holloway on 11/24/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPPulldownTableViewController.h"
#import "UIView+PPPExtensions.h"
#import <tgmath.h>

@interface PPPPulldownTableViewController ()

@end

@implementation PPPPulldownTableViewController {
    UIImageView *_pulldownArrowView;
    UITableViewCell *_pulldownConfirmation;
    
    CGFloat _pulldownConfirmationOffset;
    CGFloat _minimumContentOffset;
    
    CGFloat _pulldownEnabled;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _pulldownEnabled = YES;
    
    // Create pull-down arrow graphic
    UIImage *image = [[UIImage imageNamed:@"pulldown-tab.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _pulldownArrowView = [[UIImageView alloc] initWithImage:image];
    _pulldownArrowView.center = CGPointMake(CGRectGetWidth(self.tableView.frame) / 2,
                                            CGRectGetHeight(_pulldownArrowView.bounds) / 2);
    [self.tableView addSubview:_pulldownArrowView];
    
    // Initialize the pulldown confirmation just above the table view
    _pulldownConfirmation = [self.tableView dequeueReusableCellWithIdentifier:@"PulldownConfirmation"];
    _pulldownConfirmationOffset = -CGRectGetHeight(_pulldownConfirmation.bounds) / 2;
    _pulldownConfirmation.center = CGPointMake(CGRectGetWidth(self.tableView.frame) / 2, _pulldownConfirmationOffset);
    _pulldownConfirmation.backgroundColor = self.tableView.tintColor;
    _pulldownConfirmation.textLabel.textColor = [UIColor whiteColor];
    [self.tableView addSubview:_pulldownConfirmation];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // Though it isn't currently documented, contentInset determines initial contentOffset.
    // Use this to set the minimum contentOffset such that the user cannot scroll above the
    // pulldown confirmation. We do this in viewDidAppear because contentInset is not set
    // until after viewDidLoad.
    _minimumContentOffset = -self.tableView.contentInset.top + _pulldownConfirmationOffset * 2;
}

- (void)updatePulldown:(NSUInteger)numberOfRows {
    BOOL hidden = (numberOfRows > 0);
    [_pulldownArrowView ppp_setHidden:hidden animated:!hidden];
}

- (void)updatePulldownEnabledState {
    // Disable pulldown activation if the user scrolls down from the top of the table view.
    // This is to prevent accidental pulldown activation when the user scrolls back up.
    _pulldownEnabled = self.tableView.contentOffset.y > -self.tableView.contentInset.top ? NO : YES;
}

- (void)didActivatePulldown {}

#pragma mark - Scroll view delegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (_pulldownEnabled && scrollView.contentOffset.y <= _minimumContentOffset) {
        [self didActivatePulldown];
    }
    
    [self updatePulldownEnabledState];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self updatePulldownEnabledState];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.isDragging && scrollView.contentOffset.y < _minimumContentOffset) {
        scrollView.contentOffset = CGPointMake(0, _minimumContentOffset);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // TODO: Dispose of any resources that can be recreated.
}


@end
