//
//  PPPScrollLockTableView.h
//  Freeform
//
//  Created by Alaric Holloway on 11/24/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPPScrollLockTableView : UITableView

// If scrollLockEnabled is YES, setting contentOffset has no effect.This approach
// guarantees that no scrolling of any kind can occur. I've encountered a situation
// where the iOS keyboard will hijack a table view and cause it to scroll for no
// discernible reason. This method was the only fix.
@property (nonatomic, getter = isScrollLockEnabled) BOOL scrollLockEnabled;

@end
