//
//  PPPScrollLockTableView.m
//  Freeform
//
//  Created by Alaric Holloway on 11/24/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPScrollLockTableView.h"
#include <tgmath.h>

@implementation PPPScrollLockTableView

// Override contentOffset setter to implement scroll lock.
- (void)setContentOffset:(CGPoint)contentOffset {
    if (!self.isScrollLockEnabled) {
        [super setContentOffset:contentOffset];
    }
}

- (void)setScrollLockEnabled:(BOOL)scrollLockEnabled {
    if (!(_scrollLockEnabled = scrollLockEnabled)) {
        
        // It's possible that scrolling was locked before the scroll view had a chance to "bounce" back to
        // its original position. When scrolling is unlocked, conclude the bounce. Note that for our
        // purposes, we only need to check the top boundary.
        CGFloat y = fmax(self.contentOffset.y, -self.contentInset.top);
        
        [self setContentOffset:CGPointMake(self.contentOffset.x, y) animated:YES];
    }
}

@end
