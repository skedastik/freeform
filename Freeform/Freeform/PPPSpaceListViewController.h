//
//  PPPSpacesViewController.h
//  Freeform
//
//  Created by Alaric Holloway on 11/22/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPPEditableListViewController.h"
#import "PPPProject.h"

@interface PPPSpaceListViewController : PPPEditableListViewController <NSFetchedResultsControllerDelegate, UITextFieldDelegate, UIActionSheetDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) PPPProject *ownerProject;
@property (nonatomic, readonly) NSManagedObjectContext *managedObjectContext;

@end
