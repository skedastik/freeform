//
//  PPPSpacesViewController.m
//  Freeform
//
//  Created by Alaric Holloway on 11/22/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPAppDelegate.h"
#import "PPPSpaceListViewController.h"
#import "PPPPrimaryViewController.h"
#import "PPPPrimaryViewController+Data.h"
#import "PPPTextFieldCell.h"
#import "PPPSpace.h"

@interface PPPSpaceListViewController ()

@end

@implementation PPPSpaceListViewController {
    NSIndexPath *_indexPathForDeletion;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];

    if (self) {
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }
    
    return self;
}

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"PPPSpace" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"project == %@", self.ownerProject];
    fetchRequest.predicate = predicate;
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"dateCreated" ascending:NO];
    fetchRequest.sortDescriptors = [NSArray arrayWithObject:sort];
    
    fetchRequest.fetchBatchSize = 20;
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    self.fetchedResultsController = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}

- (NSManagedObjectContext *)managedObjectContext {
    return self.ownerProject.managedObjectContext;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = self.ownerProject.name;
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didActivatePulldown {
    [super didActivatePulldown];
    
    self.tableView.scrollLockEnabled = YES;
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"New Space", nil)
                                                        message:NSLocalizedString(@"Name your space.", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                              otherButtonTitles:NSLocalizedString(@"Done", nil), nil];
    alertView.alertViewStyle = UIAlertViewStyleDefault;
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView textFieldAtIndex:0].placeholder = NSLocalizedString(@"Unnamed Space", nil);
    [alertView show];
}

#pragma mark - UIAlert and UIActionSheet delegates

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0 && _indexPathForDeletion) {
        // Delete the space marked by the user
        NSManagedObjectContext *context = self.managedObjectContext;
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:_indexPathForDeletion]];
        
        NSError *error = nil;
        [context save:&error];
        [PPPAppDelegate handleContextSaveError:error context:context inController:self];
        
        _indexPathForDeletion = nil;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex > 0) {
        NSManagedObjectContext *context = self.managedObjectContext;
        PPPSpace *space = [NSEntityDescription insertNewObjectForEntityForName:@"PPPSpace" inManagedObjectContext:context];
        
        NSString *text = [alertView textFieldAtIndex:0].text;
        space.name = [text isEqualToString:@""] ? NSLocalizedString(@"Unnamed Space", nil) : text;
        
        [self.ownerProject addSpacesObject:space];
        
        // TODO: Drill down to theme selector and save context.
    }
    
    self.tableView.scrollLockEnabled = NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger numRows = [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
    
    [self updatePulldown:numRows];
    
    return numRows;
}

- (void)configureCell:(PPPTextFieldCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    PPPSpace *space = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textField.text = space.name;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SpaceCell";
    PPPTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textField.placeholder = NSLocalizedString(@"Unnamed Space", nil);
    cell.textField.delegate = self;
    cell.textField.ownerCell = cell;
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    PPPSpace *space = [self.fetchedResultsController objectAtIndexPath:indexPath];
    _indexPathForDeletion = indexPath;
    
    NSString *descriptionFormat = NSLocalizedString(@"Are you sure you want to delete the space named \"%@\"? Any image it contains will also be deleted.", "This string is used as a format string and must include a format specifier for the space name.");
    NSString *description = [NSString stringWithFormat:descriptionFormat, space.name];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:description
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"Delete Space", nil)
                                                    otherButtonTitles:nil];
    [actionSheet showInView:tableView];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    if ([[segue identifier] isEqualToString:@"SpaceListToWorkspace"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        PPPSpace *space = (PPPSpace *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
        
        PPPPrimaryViewController *controller = (PPPPrimaryViewController *)[segue destinationViewController];
        controller.ownerSpace = space;
    }
}

#pragma mark - UITextField delegate

- (void)textFieldDidEndEditing:(PPPTextField *)textField {
    // User has changed name of space. Validate the name string.
    if ([textField.text isEqualToString:@""]) {
        textField.text = textField.placeholder;
    }
    
    // Propagate change to managed object.
    NSIndexPath *indexPath = [self.tableView indexPathForCell:textField.ownerCell];
    PPPSpace *space = [self.fetchedResultsController objectAtIndexPath:indexPath];
    space.name = textField.text;
    
    NSError *error = nil;
    [self.managedObjectContext save:&error];
    [PPPAppDelegate handleContextSaveError:error context:self.managedObjectContext inController:self];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return NO;
}

@end
