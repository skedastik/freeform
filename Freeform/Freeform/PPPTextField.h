//
//  PPPTextField.h
//  Freeform
//
//  Created by Alaric Holloway on 11/20/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPPTextField : UITextField

@property (nonatomic, weak) UITableViewCell *ownerCell;

@end
