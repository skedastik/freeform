//
//  PPPProjectCell.h
//  Freeform
//
//  Created by Alaric Holloway on 11/20/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPPTextField.h"

@interface PPPTextFieldCell : UITableViewCell

@property (weak, nonatomic) IBOutlet PPPTextField *textField;

@end
