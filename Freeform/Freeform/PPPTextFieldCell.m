//
//  PPPProjectCell.m
//  Freeform
//
//  Created by Alaric Holloway on 11/20/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPTextFieldCell.h"

@implementation PPPTextFieldCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

// As of 1//21/2013, willTransitionToState does not consistently update. See my Stack Overflow question:
// http://stackoverflow.com/questions/20119323/willtransitiontostate-uitableviewcell-not-called-in-certain-situation
// So for now, we override setEditing.
//
// TODO: Disable name field while delete confirmation is showing

//- (void)willTransitionToState:(UITableViewCellStateMask)state {
//    [super willTransitionToState:state];
//    
////    self.nameField.enabled = !(state & UITableViewCellStateShowingDeleteConfirmationMask) && (state & UITableViewCellStateEditingMask);
//    
//    self.nameField.enabled = state & UITableViewCellStateEditingMask;
//    
//    NSLog(@"willTransitionToState");
//}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    self.textField.enabled = editing;
}

@end
