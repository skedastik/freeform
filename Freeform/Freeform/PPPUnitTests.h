//
//  PPPUnitTests.h
//  DynamicCallout
//
//  Created by Alaric Holloway on 11/11/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

// Run all unit tests
void runTests(void);

// Run an individual test, passing in a test name, and condition to test:
// true indicates success, false, failure.
void runTest(char *name, int success);
