//
//  PPPUnitTests.m
//  DynamicCallout
//
//  Created by Alaric Holloway on 11/11/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//
//  Quick and dirty unit tests.
//

#import "PPPUnitTests.h"
#import "PPPMathTest.h"

// Set doRunTests to 0 to disable unit testing.
static const int doRunTests = 0;

static int failedTests = 0;

typedef void (*testFunc)(void);

typedef struct {
    char name[128];
    testFunc func;
} test;

void runTests(void) {
    if (doRunTests) {
        printf("Running unit tests...\n\n");
        test tests[1] = {
            {"PPPMath", &runMathTests}
        };
        
        for (int i = 0; i < sizeof(tests) / sizeof(test); i++) {
            printf("%s\n\n", tests[i].name);
            (*tests[i].func)();
        }
        
        printf("\n...done. ");
        
        if (failedTests > 0) {
            printf("%d test%s failed. Failing on assert...\n\n", failedTests, failedTests > 1 ? "s" : "");
            assert(failedTests == 0);
        } else {
            printf("All tests passed.\n\n");
        }
    }
}

void runTest(char *name, int success) {
    printf("%s : %s\n", success ? "  Pass" : "> FAIL", name);
    
    if (!success) {
        failedTests++;
    }
}
