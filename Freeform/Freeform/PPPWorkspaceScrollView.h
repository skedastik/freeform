//
//  PPPWorkspaceScrollView.h
//  Freeform
//
//  Created by Alaric Holloway on 11/5/13.
//  Copyright (c) 2013 Alaric Holloway. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPPWorkspaceScrollView : UIScrollView <UIScrollViewDelegate>

@property (nonatomic, getter = isZoomEnabled) BOOL zoomEnabled;

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic) UIColor *contentViewBackgroundColor UI_APPEARANCE_SELECTOR;

- (void)setContentFrameSize:(CGSize)frameSize;

// Call this when the device orientation changes
- (void)updateMinMaxZoomScale;

// Because the user can zoom the Freeform workspace, elements will potentially
// become very small and difficult to tap. To make things easier on the user,
// we perform hit tests using a small rectangle rather than a point. This
// property returns that rect centered at the given point.
+ (CGRect)getHitTestRectCenteredAt:(CGPoint)point;

@end
