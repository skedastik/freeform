//
//  PPPWorkspaceScrollView.m
//  Freeform
//
//  Created by Alaric Holloway on 11/5/13.
//  Copyright (c) 2013 Alaric Holloway. All rights reserved.
//

#import "PPPMath.h"
#import "PPPWorkspaceScrollView.h"

// Only one workspace scroll view will exist at any given time.
static PPPWorkspaceScrollView *primaryWorkspaceView = nil;

const CGFloat PPPMaxWorkspaceWidth = 2880;
const CGFloat PPPMaxWorkspaceHeight = 2880;

const CGFloat PPPMaxZoomScale    = 1.0f;
const CGFloat PPPBaseHitTestSize = 36;


@interface PPPWorkspaceScrollView ()

@property (nonatomic) CGFloat hitTestSize;

@end


@implementation PPPWorkspaceScrollView

@dynamic contentViewBackgroundColor;

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        CGSize defaultSize = CGSizeMake(PPPMaxWorkspaceWidth, PPPMaxWorkspaceHeight);
        
        self.delegate = self;
        self.contentView = [[UIView alloc] initWithFrame:(CGRect){0, 0, defaultSize}];
        self.contentSize = defaultSize;
        
        [self updateMinMaxZoomScale];
        [self addSubview:self.contentView];
        [self updateHitTestSize];
        
        primaryWorkspaceView = self;
    }
    
    return self;
}

// Set the size of the scroll view's content view's frame
- (void)setContentFrameSize:(CGSize)frameSize {
    self.contentSize = frameSize;
    self.contentView.frame = (CGRect){0, 0, frameSize};
    [self updateMinMaxZoomScale];
}

- (void)setZoomEnabled:(BOOL)zoomEnabled {
    _zoomEnabled = zoomEnabled;
    
    if (zoomEnabled) {
        [self updateMinMaxZoomScale];
    } else {
        self.minimumZoomScale = self.zoomScale;
        self.maximumZoomScale = self.zoomScale;
    }
}

- (UIColor *)contentViewBackgroundColor {
    return nil;
}

- (void)setContentViewBackgroundColor:(UIColor *)color {
    self.contentView.backgroundColor = color;
}

- (void)updateMinMaxZoomScale {
    // The largest aspect of the content frame should fill the scroll view from edge to edge at minimum zoom
    CGFloat width = self.contentView.bounds.size.width;
    CGFloat height = self.contentView.bounds.size.height;
    
    self.minimumZoomScale = width > height ? self.frame.size.width / width : self.frame.size.height / height;
    self.maximumZoomScale = PPPMaxZoomScale;
    
    if (self.zoomScale < self.minimumZoomScale) {
        [self setZoomScale:self.minimumZoomScale animated:YES];
    }
}

- (void)updateHitTestSize {
    // The hit test rect should remain a constant size visually,
    // so adjust its size when zoom scale changes.
    self.hitTestSize = PPPBaseHitTestSize / self.zoomScale;
}

+ (CGRect)getHitTestRectCenteredAt:(CGPoint)point {
    CGFloat size = primaryWorkspaceView.hitTestSize;
    return CGRectMakeCentered(point.x, point.y, size, size);
}

# pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.contentView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    [self updateHitTestSize];
}

@end
