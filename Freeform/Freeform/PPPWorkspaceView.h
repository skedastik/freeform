//
//  PPPPrimaryView.h
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPPWorkspaceScrollView.h"
#import "PPPElementView.h"
#import "PPPGestureMode.h"
#import "PPPTheme.h"

@interface PPPWorkspaceView : UIView

- (void)initOverlayImages;
- (void)updateGestureModeButtonWithMode:(PPPGestureMode)gestureMode;
- (void)enableUndoButton:(BOOL)enableUndo andRedoButton:(BOOL)enableRedo;
- (void)setTheme:(PPPTheme *)theme;

- (void)insertElementView:(PPPElementView *)elementView atIndex:(NSUInteger)index;
- (void)exchangeElementViewAtIndex:(NSUInteger)index1 withElementViewAtIndex:(NSUInteger)index2;
- (PPPElementView *)elementViewAtZIndex:(NSUInteger)zIndex;
- (NSUInteger)zIndexOfElementView:(PPPElementView *)elementView;

- (UIView *)hitTestForViewClass:(Class)class withRecognizer:(UIGestureRecognizer *)recognizer;

- (void)enableZoomAndPan;
- (void)disableZoomAndPan;

@property (weak, nonatomic) IBOutlet PPPWorkspaceScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *overlayButtonSpaceList;
@property (weak, nonatomic) IBOutlet UIButton *overlayButtonSettings;
@property (weak, nonatomic) IBOutlet UIButton *overlayButtonNewImage;
@property (weak, nonatomic) IBOutlet UIButton *overlayButtonNewNote;
@property (weak, nonatomic) IBOutlet UIButton *overlayButtonExpand;
@property (weak, nonatomic) IBOutlet UIButton *overlayButtonUndo;
@property (weak, nonatomic) IBOutlet UIButton *overlayButtonRedo;
@property (weak, nonatomic) IBOutlet UIButton *overlayButtonPinning;
@property (weak, nonatomic) IBOutlet UIButton *overlayButtonDeleteElement;
@property (weak, nonatomic) IBOutlet UIButton *overlayButtonGestureMode;
@property (weak, nonatomic) IBOutlet UILabel *overlaySelectionCounter;

@end
