//
//  PPPPrimaryView.m
//  Freeform
//
//  Created by Alaric Holloway on 12/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPWorkspaceView.h"
#import "PPPAssetProxy.h"
#import "UIButton+PPPExtensions.h"

@class PPPPrimaryViewController;


@interface PPPWorkspaceView ()

@property (nonatomic) PPPAssetProxy *assetProxy;

@end


@implementation PPPWorkspaceView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.assetProxy = nil;
    }
    
    return self;
}

- (void)initOverlayImages {
    NSArray *buttons = @[self.overlayButtonSpaceList,
                         self.overlayButtonSettings,
                         self.overlayButtonNewImage,
                         self.overlayButtonNewNote,
                         self.overlayButtonExpand,
                         self.overlayButtonUndo,
                         self.overlayButtonRedo,
                         self.overlayButtonPinning,
                         self.overlayButtonDeleteElement,
                         self.overlayButtonGestureMode];
    
    [buttons enumerateObjectsUsingBlock:^(UIButton *button, NSUInteger idx, BOOL *stop) {
        [UIButton useTemplateRenderingModeForButton:button forState:UIControlStateNormal];
    }];
    
    [UIButton useTemplateRenderingModeForButton:self.overlayButtonGestureMode forState:UIControlStateSelected];
}

- (void)updateSelectionCounterAppearance {
    self.overlaySelectionCounter.textColor = self.overlayButtonGestureMode.selected ? [PPPAssetProxy getControlColor] : [PPPAssetProxy getBackgroundColor];
}

- (void)updateGestureModeButtonWithMode:(PPPGestureMode)gestureMode {
    self.overlayButtonGestureMode.selected = (gestureMode != PPPGestureModeManipulation);
    [self updateSelectionCounterAppearance];
}

- (void)enableUndoButton:(BOOL)enableUndo andRedoButton:(BOOL)enableRedo {
    self.overlayButtonUndo.enabled = enableUndo;
    self.overlayButtonRedo.enabled = enableRedo;
}

- (void)setTheme:(PPPTheme *)theme {
    self.assetProxy = [[PPPAssetProxy alloc] initWithTheme:theme];

    [self updateSelectionCounterAppearance];
    [[PPPWorkspaceView appearance] setBackgroundColor:[PPPAssetProxy getSuperBackgroundColor]];
    [[PPPWorkspaceScrollView appearance] setContentViewBackgroundColor:[PPPAssetProxy getBackgroundColor]];
    [[PPPElementView appearance] setRefreshedAppearance:nil];
    self.tintColor = [PPPAssetProxy getControlColor];
    
    if (self.superview) {
        // TODO: Remove and add self to hierarchy so new UIAppearance kicks in.
    }
}

- (void)insertElementView:(PPPElementView *)elementView atIndex:(NSUInteger)index {
    [self.scrollView.contentView insertSubview:elementView atIndex:index];
}

- (void)exchangeElementViewAtIndex:(NSUInteger)index1 withElementViewAtIndex:(NSUInteger)index2 {
    [self.scrollView.contentView exchangeSubviewAtIndex:index1 withSubviewAtIndex:index2];
}

- (PPPElementView *)elementViewAtZIndex:(NSUInteger)zIndex {
    return self.scrollView.contentView.subviews[zIndex];
}

- (NSUInteger)zIndexOfElementView:(PPPElementView *)elementView {
    return [self.scrollView.contentView.subviews indexOfObject:elementView];
}

- (UIView *)hitTestForViewClass:(Class)class withRecognizer:(UIGestureRecognizer *)recognizer {
    UIView *hitView = [self hitTest:[recognizer locationOfTouch:0 inView:self] withEvent:nil];
    
    if ([hitView isKindOfClass:class]) {
        return (UIView *)hitView;
    }
    
    return nil;
}

- (void)enableZoomAndPan {
    self.scrollView.scrollEnabled = YES;
    self.scrollView.zoomEnabled = YES;
}

- (void)disableZoomAndPan {
    self.scrollView.scrollEnabled = NO;
    self.scrollView.zoomEnabled = NO;
}

@end
