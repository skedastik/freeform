//
//  UIButton+PPPExtensions.h
//  Freeform
//
//  Created by Alaric Holloway on 11/19/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (PPPExtensions)

+ (void)useTemplateRenderingModeForButton:(UIButton *)button forState:(UIControlState)state;

@end
