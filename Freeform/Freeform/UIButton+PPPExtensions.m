//
//  UIButton+PPPExtensions.m
//  Freeform
//
//  Created by Alaric Holloway on 11/19/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "UIButton+PPPExtensions.h"

@implementation UIButton (PPPExtensions)

+ (void)useTemplateRenderingModeForButton:(UIButton *)button forState:(UIControlState)state {
    UIImage *templateImage = [[button imageForState:state] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [button setImage:templateImage forState:state];
}

@end
