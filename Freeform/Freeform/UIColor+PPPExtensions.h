//
//  UIColor_PPPExtensions.h
//  Freeform
//
//  Created by Alaric Holloway on 11/19/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PPPExtensions)

// Return an NSString representation of UIColor (in CIColor's string representation format)
- (NSString *)ppp_toColorString;

// Return a UIColor from the given string (in CIColor's string representation format)
+ (UIColor *)ppp_fromColorString:(NSString *)colorString;

@end
