//
//  UIColor_PPPExtensions.m
//  Freeform
//
//  Created by Alaric Holloway on 11/19/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "UIColor+PPPExtensions.h"

@implementation UIColor (PPPExtensions)

- (NSString *)ppp_toColorString {
    return [CIColor colorWithCGColor:self.CGColor].stringRepresentation;
}

+ (UIColor *)ppp_fromColorString:(NSString *)colorString {
    // As of 12/10/2013, UIColor's colorWithCIColor method returns a UICIColor object which
    // raises an exception when passed to a UILabel object's textColor setter. For this
    // reason, we use CIColor with UIColor's colorWithRed:green:blue:alpha method.
    CIColor *color = [CIColor colorWithString:colorString];
    return [UIColor colorWithRed:color.red green:color.green blue:color.blue alpha:color.alpha];
}

@end
