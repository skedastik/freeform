//
//  UIImage+PPPExtensions.h
//  Freeform
//
//  Created by Alaric Holloway on 12/10/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (PPPExtensions)

- (UIImage *)imageWithTint:(UIColor *)tintColor;

@end
