//
//  UIImage+PPPExtensions.m
//  Freeform
//
//  Created by Alaric Holloway on 12/10/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "UIImage+PPPExtensions.h"

@implementation UIImage (PPPExtensions)

- (UIImage *)imageWithTint:(UIColor *)tintColor {
    UIGraphicsBeginImageContextWithOptions(self.size, NO, 0);
    
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    [self drawInRect:rect];
    [tintColor set];
    UIRectFillUsingBlendMode(rect, kCGBlendModeSourceAtop);
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return tintedImage;
}

@end
