//
//  UIView_PPPShineAnimation.h
//  Freeform
//
//  Created by Alaric Holloway on 11/25/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (PPPExtensions)

// setHidden with an animated option
- (void)ppp_setHidden:(BOOL)hidden animated:(BOOL)animated;

// Returns CGRect encompassing all touch points in the given recognizer. This rect is undefined if
// the recognizer reports zero touch points.
+ (CGRect)touchRectFromGestureRecognizer:(UIGestureRecognizer *)recognizer;

@end
