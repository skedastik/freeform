//
//  UIView_PPPShineAnimation.m
//  Freeform
//
//  Created by Alaric Holloway on 11/25/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "UIView+PPPExtensions.h"
#import "PPPMath.h"

static NSString * const shineAnimationKey = @"shineAnimation";

@implementation UIView (PPPExtensions)

- (void)ppp_setHidden:(BOOL)hidden animated:(BOOL)animated {
    if (hidden != self.hidden) {
        if (animated) {
            [UIView transitionWithView:self
                              duration:0.5
                               options:UIViewAnimationOptionTransitionCrossDissolve | UIViewAnimationCurveEaseIn
                            animations:NULL
                            completion:NULL];
        }
        
        self.hidden = hidden;
    }
}

+ (CGRect)touchRectFromGestureRecognizer:(UIGestureRecognizer *)recognizer {
    CGRect touchRect;
    CGPoint tpMin, tpMax;
    
    if (recognizer.numberOfTouches > 0) {
        tpMin = [recognizer locationOfTouch:0 inView:recognizer.view];
        tpMax = tpMin;
        
        for (int i = 1; i < recognizer.numberOfTouches; i++) {
            CGPoint tp = [recognizer locationOfTouch:i inView:recognizer.view];
            tpMin = CGPointMake(fmin(tpMin.x, tp.x), fmin(tpMin.y, tp.y));
            tpMax = CGPointMake(fmax(tpMax.x, tp.x), fmax(tpMax.y, tp.y));
        }
    }
    
    touchRect = CGRectFromTwoPoints(tpMin, tpMax);
    
    return touchRect;
}

@end
