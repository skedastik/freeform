//
//  main.m
//  Freeform
//
//  Created by Alaric Holloway on 11/9/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PPPAppDelegate.h"
#import "PPPUnitTests.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        runTests();
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PPPAppDelegate class]));
    }
}
