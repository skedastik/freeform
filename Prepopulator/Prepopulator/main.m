//
//  main.m
//  Prepopulator
//
//  Created by Alaric Holloway on 11/18/13.
//  Copyright (c) 2013 Paper Pencil Pixel. All rights reserved.
//

#import "PPPProject.h"
#import "PPPSpace.h"
#import "PPPTheme.h"
#import "PPPSwatch.h"
#import "PPPNote.h"
#import "PPPNoteTail.h"

static NSManagedObjectModel *managedObjectModel()
{
    static NSManagedObjectModel *model = nil;
    if (model != nil) {
        return model;
    }
    
    NSString *path = @"Freeform";
    path = [path stringByDeletingPathExtension];
    NSURL *modelURL = [NSURL fileURLWithPath:[path stringByAppendingPathExtension:@"momd"]];
    model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return model;
}

static NSManagedObjectContext *managedObjectContext()
{
    static NSManagedObjectContext *context = nil;
    if (context != nil) {
        return context;
    }

    @autoreleasepool {
        context = [[NSManagedObjectContext alloc] init];
        
        NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:managedObjectModel()];
        [context setPersistentStoreCoordinator:coordinator];
        
        NSString *STORE_TYPE = NSSQLiteStoreType;
        
        NSString *path = [[NSProcessInfo processInfo] arguments][0];
        path = [path stringByDeletingPathExtension];
        NSURL *url = [NSURL fileURLWithPath:[path stringByAppendingPathExtension:@"sqlite"]];
        
        // Use journal_mode suitable for read-only persistent store
        NSError *error;
        NSPersistentStore *newStore = [coordinator addPersistentStoreWithType:STORE_TYPE configuration:nil URL:url options:@{NSSQLitePragmasOption:@{ @"journal_mode" : @"DELETE" }} error:&error];
        
        if (newStore == nil) {
            NSLog(@"Store Configuration Failure %@", ([error localizedDescription] != nil) ? [error localizedDescription] : @"Unknown Error");
        }
    }
    return context;
}

void prepopulateDefaults() {
    NSManagedObjectContext *context = managedObjectContext();
    
    NSArray *swatchArray = @[[NSEntityDescription insertNewObjectForEntityForName:@"PPPSwatch" inManagedObjectContext:context],
                             [NSEntityDescription insertNewObjectForEntityForName:@"PPPSwatch" inManagedObjectContext:context],
                             [NSEntityDescription insertNewObjectForEntityForName:@"PPPSwatch" inManagedObjectContext:context],
                             [NSEntityDescription insertNewObjectForEntityForName:@"PPPSwatch" inManagedObjectContext:context],
                             [NSEntityDescription insertNewObjectForEntityForName:@"PPPSwatch" inManagedObjectContext:context],
                             [NSEntityDescription insertNewObjectForEntityForName:@"PPPSwatch" inManagedObjectContext:context],
                             [NSEntityDescription insertNewObjectForEntityForName:@"PPPSwatch" inManagedObjectContext:context],
                             [NSEntityDescription insertNewObjectForEntityForName:@"PPPSwatch" inManagedObjectContext:context],
                             [NSEntityDescription insertNewObjectForEntityForName:@"PPPSwatch" inManagedObjectContext:context],
                             [NSEntityDescription insertNewObjectForEntityForName:@"PPPSwatch" inManagedObjectContext:context],
                             [NSEntityDescription insertNewObjectForEntityForName:@"PPPSwatch" inManagedObjectContext:context],
                             [NSEntityDescription insertNewObjectForEntityForName:@"PPPSwatch" inManagedObjectContext:context]];
    ((PPPSwatch *)swatchArray[0]).color  = @"1.0   1.0   1.0   1.0";
    ((PPPSwatch *)swatchArray[1]).color  = @"0.0   0.0   0.0   1.0";
    ((PPPSwatch *)swatchArray[2]).color  = @"0.25  0.25  0.25  1.0";
    ((PPPSwatch *)swatchArray[3]).color  = @"0.5   0.5   0.5   1.0";
    ((PPPSwatch *)swatchArray[4]).color  = @"0.75  0.75  0.75  1.0";
    ((PPPSwatch *)swatchArray[5]).color  = @"1.0   0.0   0.0   1.0";
    ((PPPSwatch *)swatchArray[6]).color  = @"0.0   1.0   0.0   1.0";
    ((PPPSwatch *)swatchArray[7]).color  = @"0.0   0.0   1.0   1.0";
    ((PPPSwatch *)swatchArray[8]).color  = @"1.0   1.0   0.0   1.0";
    ((PPPSwatch *)swatchArray[9]).color  = @"0.0   1.0   1.0   1.0";
    ((PPPSwatch *)swatchArray[10]).color = @"1.0   0.0   1.0   1.0";
    ((PPPSwatch *)swatchArray[11]).color = @"0.0   0.0   0.0   0.0";
    ((PPPSwatch *)swatchArray[2]).detailColor  = @"1.0   1.0   1.0   1.0";
    ((PPPSwatch *)swatchArray[7]).detailColor  = @"1.0   1.0   1.0   1.0";
    NSOrderedSet *swatchSet = [[NSOrderedSet alloc] initWithArray:swatchArray];
    
    PPPTheme *theme = [NSEntityDescription insertNewObjectForEntityForName:@"PPPTheme" inManagedObjectContext:context];
    theme.name = @"Light";
    theme.backgroundColor      = @"1.0   1.0   0.95   1.0";
    theme.superBackgroundColor = @"0.9   0.9   0.9   1.0";
    theme.controlColor         = @"0.0   0.0   0.0   1.0";
    theme.selectionColor       = @"1.0   0.0   0.0   1.0";
    theme.defaultTextColor     = @"0.0   0.0   0.0   1.0";
    [theme addSwatches:swatchSet];
    
    PPPNoteTail *noteTail = [NSEntityDescription insertNewObjectForEntityForName:@"PPPNoteTail" inManagedObjectContext:context];
    noteTail.position = @"{0,0}";
    
    PPPNote *note2 = [NSEntityDescription insertNewObjectForEntityForName:@"PPPNote" inManagedObjectContext:context];
    note2.center = @"{210,300}";
    note2.pinned = NO;
    note2.zIndex = 1;
    note2.swatchIndex = 2;
    note2.width = 200;
    note2.height = 40;
    note2.text = @"2nd Note (OutLink)";
    
    PPPNote *note1 = [NSEntityDescription insertNewObjectForEntityForName:@"PPPNote" inManagedObjectContext:context];
    note1.center = @"{120,100}";
    note1.pinned = NO;
    note1.zIndex = 0;
    note1.swatchIndex = 4;
    note1.width = 120;
    note1.height = 40;
    note1.text = @"First Note";
    [note1 addOutLinksObject:note2];
    [note1 addTailsObject:noteTail];
    
    PPPNote *note3 = [NSEntityDescription insertNewObjectForEntityForName:@"PPPNote" inManagedObjectContext:context];
    note3.center = @"{120,400}";
    note3.pinned = NO;
    note3.zIndex = 2;
    note3.swatchIndex = 7;
    note3.width = 200;
    note3.height = 40;
    note3.text = @"3rd Note (InLink)";
    [note3 addOutLinksObject:note1];
    
    PPPSpace *space = [NSEntityDescription insertNewObjectForEntityForName:@"PPPSpace" inManagedObjectContext:context];
    space.name = @"Unnamed Space";
    space.width = 1440;
    space.height = 900;
    space.theme = theme;
    space.dateCreated = [[NSDate date] timeIntervalSinceReferenceDate];
    [space addElementsObject:note1];
    [space addElementsObject:note2];
    [space addElementsObject:note3];
    
    PPPProject *project = [NSEntityDescription insertNewObjectForEntityForName:@"PPPProject" inManagedObjectContext:context];
    project.name = @"Unnamed Project";
    project.dateCreated = [[NSDate date] timeIntervalSinceReferenceDate];
    [project addSpacesObject:space];
}

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        // Create the managed object context
        NSManagedObjectContext *context = managedObjectContext();
        
        prepopulateDefaults();
        
        // Save the managed object context
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Error while saving: %@ [%@]", ([error localizedDescription] != nil) ? [error localizedDescription] : @"Unknown Error", [error userInfo]);
            exit(1);
        }
        
        NSLog(@"Prepopulate finished.");
    }
    return 0;
}

