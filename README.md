# README #

Freeform is an iPhone app for visual thinkers and designers; a virtual, networkable whiteboard and more. I was doing a lot of design work on paper at the time and needed a tool to bridge the gap between between my "analog" workflow and my digital workflow.